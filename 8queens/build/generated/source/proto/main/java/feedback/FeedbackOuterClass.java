// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: feedback.proto

package feedback;

public final class FeedbackOuterClass {
  private FeedbackOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  public interface FeedbackOrBuilder extends
      // @@protoc_insertion_point(interface_extends:feedback.Feedback)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <code>optional int32 isSuccess = 1;</code>
     */
    int getIsSuccess();

    /**
     * <code>optional string error = 2;</code>
     */
    java.lang.String getError();
    /**
     * <code>optional string error = 2;</code>
     */
    com.google.protobuf.ByteString
        getErrorBytes();
  }
  /**
   * Protobuf type {@code feedback.Feedback}
   */
  public  static final class Feedback extends
      com.google.protobuf.GeneratedMessageV3 implements
      // @@protoc_insertion_point(message_implements:feedback.Feedback)
      FeedbackOrBuilder {
    // Use Feedback.newBuilder() to construct.
    private Feedback(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
      super(builder);
    }
    private Feedback() {
      isSuccess_ = 0;
      error_ = "";
    }

    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
    }
    private Feedback(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      int mutable_bitField0_ = 0;
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            default: {
              if (!input.skipField(tag)) {
                done = true;
              }
              break;
            }
            case 8: {

              isSuccess_ = input.readInt32();
              break;
            }
            case 18: {
              java.lang.String s = input.readStringRequireUtf8();

              error_ = s;
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return feedback.FeedbackOuterClass.internal_static_feedback_Feedback_descriptor;
    }

    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return feedback.FeedbackOuterClass.internal_static_feedback_Feedback_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              feedback.FeedbackOuterClass.Feedback.class, feedback.FeedbackOuterClass.Feedback.Builder.class);
    }

    public static final int ISSUCCESS_FIELD_NUMBER = 1;
    private int isSuccess_;
    /**
     * <code>optional int32 isSuccess = 1;</code>
     */
    public int getIsSuccess() {
      return isSuccess_;
    }

    public static final int ERROR_FIELD_NUMBER = 2;
    private volatile java.lang.Object error_;
    /**
     * <code>optional string error = 2;</code>
     */
    public java.lang.String getError() {
      java.lang.Object ref = error_;
      if (ref instanceof java.lang.String) {
        return (java.lang.String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        error_ = s;
        return s;
      }
    }
    /**
     * <code>optional string error = 2;</code>
     */
    public com.google.protobuf.ByteString
        getErrorBytes() {
      java.lang.Object ref = error_;
      if (ref instanceof java.lang.String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        error_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    private byte memoizedIsInitialized = -1;
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (isSuccess_ != 0) {
        output.writeInt32(1, isSuccess_);
      }
      if (!getErrorBytes().isEmpty()) {
        com.google.protobuf.GeneratedMessageV3.writeString(output, 2, error_);
      }
    }

    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (isSuccess_ != 0) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt32Size(1, isSuccess_);
      }
      if (!getErrorBytes().isEmpty()) {
        size += com.google.protobuf.GeneratedMessageV3.computeStringSize(2, error_);
      }
      memoizedSize = size;
      return size;
    }

    private static final long serialVersionUID = 0L;
    @java.lang.Override
    public boolean equals(final java.lang.Object obj) {
      if (obj == this) {
       return true;
      }
      if (!(obj instanceof feedback.FeedbackOuterClass.Feedback)) {
        return super.equals(obj);
      }
      feedback.FeedbackOuterClass.Feedback other = (feedback.FeedbackOuterClass.Feedback) obj;

      boolean result = true;
      result = result && (getIsSuccess()
          == other.getIsSuccess());
      result = result && getError()
          .equals(other.getError());
      return result;
    }

    @java.lang.Override
    public int hashCode() {
      if (memoizedHashCode != 0) {
        return memoizedHashCode;
      }
      int hash = 41;
      hash = (19 * hash) + getDescriptorForType().hashCode();
      hash = (37 * hash) + ISSUCCESS_FIELD_NUMBER;
      hash = (53 * hash) + getIsSuccess();
      hash = (37 * hash) + ERROR_FIELD_NUMBER;
      hash = (53 * hash) + getError().hashCode();
      hash = (29 * hash) + unknownFields.hashCode();
      memoizedHashCode = hash;
      return hash;
    }

    public static feedback.FeedbackOuterClass.Feedback parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static feedback.FeedbackOuterClass.Feedback parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static feedback.FeedbackOuterClass.Feedback parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static feedback.FeedbackOuterClass.Feedback parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static feedback.FeedbackOuterClass.Feedback parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static feedback.FeedbackOuterClass.Feedback parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static feedback.FeedbackOuterClass.Feedback parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static feedback.FeedbackOuterClass.Feedback parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static feedback.FeedbackOuterClass.Feedback parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static feedback.FeedbackOuterClass.Feedback parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(feedback.FeedbackOuterClass.Feedback prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code feedback.Feedback}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:feedback.Feedback)
        feedback.FeedbackOuterClass.FeedbackOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return feedback.FeedbackOuterClass.internal_static_feedback_Feedback_descriptor;
      }

      protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return feedback.FeedbackOuterClass.internal_static_feedback_Feedback_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                feedback.FeedbackOuterClass.Feedback.class, feedback.FeedbackOuterClass.Feedback.Builder.class);
      }

      // Construct using feedback.FeedbackOuterClass.Feedback.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessageV3
                .alwaysUseFieldBuilders) {
        }
      }
      public Builder clear() {
        super.clear();
        isSuccess_ = 0;

        error_ = "";

        return this;
      }

      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return feedback.FeedbackOuterClass.internal_static_feedback_Feedback_descriptor;
      }

      public feedback.FeedbackOuterClass.Feedback getDefaultInstanceForType() {
        return feedback.FeedbackOuterClass.Feedback.getDefaultInstance();
      }

      public feedback.FeedbackOuterClass.Feedback build() {
        feedback.FeedbackOuterClass.Feedback result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      public feedback.FeedbackOuterClass.Feedback buildPartial() {
        feedback.FeedbackOuterClass.Feedback result = new feedback.FeedbackOuterClass.Feedback(this);
        result.isSuccess_ = isSuccess_;
        result.error_ = error_;
        onBuilt();
        return result;
      }

      public Builder clone() {
        return (Builder) super.clone();
      }
      public Builder setField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          Object value) {
        return (Builder) super.setField(field, value);
      }
      public Builder clearField(
          com.google.protobuf.Descriptors.FieldDescriptor field) {
        return (Builder) super.clearField(field);
      }
      public Builder clearOneof(
          com.google.protobuf.Descriptors.OneofDescriptor oneof) {
        return (Builder) super.clearOneof(oneof);
      }
      public Builder setRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          int index, Object value) {
        return (Builder) super.setRepeatedField(field, index, value);
      }
      public Builder addRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          Object value) {
        return (Builder) super.addRepeatedField(field, value);
      }
      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof feedback.FeedbackOuterClass.Feedback) {
          return mergeFrom((feedback.FeedbackOuterClass.Feedback)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(feedback.FeedbackOuterClass.Feedback other) {
        if (other == feedback.FeedbackOuterClass.Feedback.getDefaultInstance()) return this;
        if (other.getIsSuccess() != 0) {
          setIsSuccess(other.getIsSuccess());
        }
        if (!other.getError().isEmpty()) {
          error_ = other.error_;
          onChanged();
        }
        onChanged();
        return this;
      }

      public final boolean isInitialized() {
        return true;
      }

      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        feedback.FeedbackOuterClass.Feedback parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (feedback.FeedbackOuterClass.Feedback) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      private int isSuccess_ ;
      /**
       * <code>optional int32 isSuccess = 1;</code>
       */
      public int getIsSuccess() {
        return isSuccess_;
      }
      /**
       * <code>optional int32 isSuccess = 1;</code>
       */
      public Builder setIsSuccess(int value) {
        
        isSuccess_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>optional int32 isSuccess = 1;</code>
       */
      public Builder clearIsSuccess() {
        
        isSuccess_ = 0;
        onChanged();
        return this;
      }

      private java.lang.Object error_ = "";
      /**
       * <code>optional string error = 2;</code>
       */
      public java.lang.String getError() {
        java.lang.Object ref = error_;
        if (!(ref instanceof java.lang.String)) {
          com.google.protobuf.ByteString bs =
              (com.google.protobuf.ByteString) ref;
          java.lang.String s = bs.toStringUtf8();
          error_ = s;
          return s;
        } else {
          return (java.lang.String) ref;
        }
      }
      /**
       * <code>optional string error = 2;</code>
       */
      public com.google.protobuf.ByteString
          getErrorBytes() {
        java.lang.Object ref = error_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (java.lang.String) ref);
          error_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <code>optional string error = 2;</code>
       */
      public Builder setError(
          java.lang.String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  
        error_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>optional string error = 2;</code>
       */
      public Builder clearError() {
        
        error_ = getDefaultInstance().getError();
        onChanged();
        return this;
      }
      /**
       * <code>optional string error = 2;</code>
       */
      public Builder setErrorBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
        
        error_ = value;
        onChanged();
        return this;
      }
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }

      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }


      // @@protoc_insertion_point(builder_scope:feedback.Feedback)
    }

    // @@protoc_insertion_point(class_scope:feedback.Feedback)
    private static final feedback.FeedbackOuterClass.Feedback DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new feedback.FeedbackOuterClass.Feedback();
    }

    public static feedback.FeedbackOuterClass.Feedback getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<Feedback>
        PARSER = new com.google.protobuf.AbstractParser<Feedback>() {
      public Feedback parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
          return new Feedback(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<Feedback> parser() {
      return PARSER;
    }

    @java.lang.Override
    public com.google.protobuf.Parser<Feedback> getParserForType() {
      return PARSER;
    }

    public feedback.FeedbackOuterClass.Feedback getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_feedback_Feedback_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_feedback_Feedback_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\016feedback.proto\022\010feedback\",\n\010Feedback\022\021" +
      "\n\tisSuccess\030\001 \001(\005\022\r\n\005error\030\002 \001(\tb\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
    internal_static_feedback_Feedback_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_feedback_Feedback_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_feedback_Feedback_descriptor,
        new java.lang.String[] { "IsSuccess", "Error", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
