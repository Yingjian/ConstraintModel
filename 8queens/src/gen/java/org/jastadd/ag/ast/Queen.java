/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\ag.ast:41
 * @astdecl Queen : ASTNode ::= <Row:int> <Column:int>;
 * @production Queen : {@link ASTNode} ::= <span class="component">&lt;Row:{@link int}&gt;</span> <span class="component">&lt;Column:{@link int}&gt;</span>;

 */
public class Queen extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:483
   */
  public static Queen createRef(String ref) {
    Unresolved$Queen unresolvedNode = new Unresolved$Queen();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:489
   */
  public static Queen createRefDirection(String ref) {
    Unresolved$Queen unresolvedNode = new Unresolved$Queen();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:677
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1825
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1831
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Queen() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    state().enterConstruction();
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Row", "Column"},
    type = {"int", "int"},
    kind = {"Token", "Token"}
  )
  public Queen(int p0, int p1) {
state().enterConstruction();
    setRow(p0);
    setColumn(p1);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:26
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:34
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:38
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:44
   */
  public Queen clone() throws CloneNotSupportedException {
    Queen node = (Queen) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:49
   */
  public Queen copy() {
    try {
      Queen node = (Queen) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:73
   */
  @Deprecated
  public Queen fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:83
   */
  public Queen treeCopyNoTransform() {
    Queen tree = (Queen) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:104
   */
  public Queen treeCopy() {
    Queen tree = (Queen) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:119
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:122
   */
  protected void inc_copyHandlers(Queen copy) {
    super.inc_copyHandlers(copy);

        if (getRow_handler != null) {
          copy.getRow_handler = ASTNode$DepGraphNode.createAstHandler(getRow_handler, copy);
        }
        if (getColumn_handler != null) {
          copy.getColumn_handler = ASTNode$DepGraphNode.createAstHandler(getColumn_handler, copy);
        }
  }
  /** @apilevel internal 
   * @declaredat ASTNode:134
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:141
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:143
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  if (getRow_handler != null) {
    getRow_handler.throwAway();
  }
  if (getColumn_handler != null) {
    getColumn_handler.throwAway();
  }
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:158
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:159
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  if (getRow_handler != null) {
    getRow_handler.cleanupListeners();
  }
  if (getColumn_handler != null) {
    getColumn_handler.cleanupListeners();
  }
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:173
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:174
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   */
  protected ASTNode$DepGraphNode getRow_handler = ASTNode$DepGraphNode.createAstHandler(this, "getRow", null);
  /**
   * Replaces the lexeme Row.
   * @param value The new value for the lexeme Row.
   * @apilevel high-level
   */
  public Queen setRow(int value) {
    tokenint_Row = value;
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      getRow_handler.notifyDependencies();
    
    
    
    
    }
    return this;
  }
  /** @apilevel internal 
   */
  protected int tokenint_Row;
  /**
   * Retrieves the value for the lexeme Row.
   * @return The value for the lexeme Row.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Row")
  public int getRow() {
    
    state().addHandlerDepTo(getRow_handler);
    return tokenint_Row;
  }
  /**
   */
  protected ASTNode$DepGraphNode getColumn_handler = ASTNode$DepGraphNode.createAstHandler(this, "getColumn", null);
  /**
   * Replaces the lexeme Column.
   * @param value The new value for the lexeme Column.
   * @apilevel high-level
   */
  public Queen setColumn(int value) {
    tokenint_Column = value;
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      getColumn_handler.notifyDependencies();
    
    
    
    
    }
    return this;
  }
  /** @apilevel internal 
   */
  protected int tokenint_Column;
  /**
   * Retrieves the value for the lexeme Column.
   * @return The value for the lexeme Column.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Column")
  public int getColumn() {
    
    state().addHandlerDepTo(getColumn_handler);
    return tokenint_Column;
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
