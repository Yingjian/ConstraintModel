/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\ag.ast:40
 * @astdecl Eight_Queens : ASTNode ::= Queen* <Num:int> <Counter:int>;
 * @production Eight_Queens : {@link ASTNode} ::= <span class="component">{@link Queen}*</span> <span class="component">&lt;Num:{@link int}&gt;</span> <span class="component">&lt;Counter:{@link int}&gt;</span>;

 */
public class Eight_Queens extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect init
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\main\\jastadd\\UseCase\\init.jadd:2
   */
  public void init(int amount){
        this.setNum(amount);
        Queen[] Q = new Queen[this.getNum()];
        for(int i=0; i<this.getNum(); i++){
            
            Q[i] = new Queen();
            Q[i].setRow(i);
            this.addQueen(Q[i]);
        }
    }
  /**
   * @aspect init
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\main\\jastadd\\UseCase\\init.jadd:12
   */
  public boolean findQueen(int i, int amountofQueens, Constraint_Model CM){//find queen node
        
        
        if(i>amountofQueens-1){//solution
            this.setCounter(this.getCounter()+1);  
            print(amountofQueens);//print
            return true;
        }
        for(int m=0;m<amountofQueens;m++){//DP
            //System.out.println("now check the queen on row "+ i +" and column " + m);
            this.getQueen(i).setColumn(m);
            if(check(this.getQueen(i), CM)){//check validity
                
                //System.out.println("mmmmmmmmm" + m);
                this.findQueen(i+1, amountofQueens, CM);
                this.getQueen(i).setColumn(0);//reset
                }
        }
        return false;
    }
  /**
   * @aspect init
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\main\\jastadd\\UseCase\\init.jadd:33
   */
  public boolean check(Queen Q, Constraint_Model CM){//check validity
        for(int i=0; i<Q.getRow(); i++){
           // System.out.println("check "+ i +" " + this.getQueen(i).getColumn()+" and " + Q.getRow() +" "+Q.getColumn());
            if(CM.Check(this.getQueen(i), Q)){
                return false;
            }
        }
        return true;
    }
  /**
   * @aspect init
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\main\\jastadd\\UseCase\\init.jadd:43
   */
  public void print(int amountofQueens){//print
        System.out.print("Solution"+this.getCounter()+":"+"\n");
        for(int i=0;i<amountofQueens;i++){
            for(int m=0;m<amountofQueens;m++){
                if(this.getQueen(i).getColumn()==m){  
                    System.out.print("o ");
                }
                else{
                        System.out.print("+ ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:471
   */
  public static Eight_Queens createRef(String ref) {
    Unresolved$Eight_Queens unresolvedNode = new Unresolved$Eight_Queens();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:477
   */
  public static Eight_Queens createRefDirection(String ref) {
    Unresolved$Eight_Queens unresolvedNode = new Unresolved$Eight_Queens();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:673
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1797
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1803
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Eight_Queens() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[1];  getChild_handler = new ASTNode$DepGraphNode[children.length];
    state().enterConstruction();
    setChild(new JastAddList(), 0);
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:16
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Queen", "Num", "Counter"},
    type = {"JastAddList<Queen>", "int", "int"},
    kind = {"List", "Token", "Token"}
  )
  public Eight_Queens(JastAddList<Queen> p0, int p1, int p2) {
state().enterConstruction();
    setChild(p0, 0);
    setNum(p1);
    setCounter(p2);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:29
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:37
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:41
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:44
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:47
   */
  public Eight_Queens clone() throws CloneNotSupportedException {
    Eight_Queens node = (Eight_Queens) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:52
   */
  public Eight_Queens copy() {
    try {
      Eight_Queens node = (Eight_Queens) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:76
   */
  @Deprecated
  public Eight_Queens fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:86
   */
  public Eight_Queens treeCopyNoTransform() {
    Eight_Queens tree = (Eight_Queens) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:107
   */
  public Eight_Queens treeCopy() {
    Eight_Queens tree = (Eight_Queens) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:122
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:125
   */
  protected void inc_copyHandlers(Eight_Queens copy) {
    super.inc_copyHandlers(copy);

        if (getNum_handler != null) {
          copy.getNum_handler = ASTNode$DepGraphNode.createAstHandler(getNum_handler, copy);
        }
        if (getCounter_handler != null) {
          copy.getCounter_handler = ASTNode$DepGraphNode.createAstHandler(getCounter_handler, copy);
        }
  }
  /** @apilevel internal 
   * @declaredat ASTNode:137
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:144
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:146
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  if (getNum_handler != null) {
    getNum_handler.throwAway();
  }
  if (getCounter_handler != null) {
    getCounter_handler.throwAway();
  }
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:161
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:162
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  if (getNum_handler != null) {
    getNum_handler.cleanupListeners();
  }
  if (getCounter_handler != null) {
    getCounter_handler.cleanupListeners();
  }
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:176
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:177
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   * Replaces the Queen list.
   * @param list The new list node to be used as the Queen list.
   * @apilevel high-level
   */
  public Eight_Queens setQueenList(JastAddList<Queen> list) {
    setChild(list, 0);
    return this;
  }
  /**
   * Retrieves the number of children in the Queen list.
   * @return Number of children in the Queen list.
   * @apilevel high-level
   */
  public int getNumQueen() {
    return getQueenList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Queen list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Queen list.
   * @apilevel low-level
   */
  public int getNumQueenNoTransform() {
    return getQueenListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Queen list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Queen list.
   * @apilevel high-level
   */
  public Queen getQueen(int i) {
    return (Queen) getQueenList().getChild(i);
  }
  /**
   * Check whether the Queen list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasQueen() {
    return getQueenList().getNumChild() != 0;
  }
  /**
   * Append an element to the Queen list.
   * @param node The element to append to the Queen list.
   * @apilevel high-level
   */
  public Eight_Queens addQueen(Queen node) {
    JastAddList<Queen> list = (parent == null) ? getQueenListNoTransform() : getQueenList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public Eight_Queens addQueenNoTransform(Queen node) {
    JastAddList<Queen> list = getQueenListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the Queen list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public Eight_Queens setQueen(Queen node, int i) {
    JastAddList<Queen> list = getQueenList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the Queen list.
   * @return The node representing the Queen list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Queen")
  public JastAddList<Queen> getQueenList() {
    JastAddList<Queen> list = (JastAddList<Queen>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Queen list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Queen list.
   * @apilevel low-level
   */
  public JastAddList<Queen> getQueenListNoTransform() {
    return (JastAddList<Queen>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Queen list without
   * triggering rewrites.
   */
  public Queen getQueenNoTransform(int i) {
    return (Queen) getQueenListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Queen list.
   * @return The node representing the Queen list.
   * @apilevel high-level
   */
  public JastAddList<Queen> getQueens() {
    return getQueenList();
  }
  /**
   * Retrieves the Queen list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Queen list.
   * @apilevel low-level
   */
  public JastAddList<Queen> getQueensNoTransform() {
    return getQueenListNoTransform();
  }
  /**
   */
  protected ASTNode$DepGraphNode getNum_handler = ASTNode$DepGraphNode.createAstHandler(this, "getNum", null);
  /**
   * Replaces the lexeme Num.
   * @param value The new value for the lexeme Num.
   * @apilevel high-level
   */
  public Eight_Queens setNum(int value) {
    tokenint_Num = value;
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      getNum_handler.notifyDependencies();
    
    
    
    
    }
    return this;
  }
  /** @apilevel internal 
   */
  protected int tokenint_Num;
  /**
   * Retrieves the value for the lexeme Num.
   * @return The value for the lexeme Num.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Num")
  public int getNum() {
    
    state().addHandlerDepTo(getNum_handler);
    return tokenint_Num;
  }
  /**
   */
  protected ASTNode$DepGraphNode getCounter_handler = ASTNode$DepGraphNode.createAstHandler(this, "getCounter", null);
  /**
   * Replaces the lexeme Counter.
   * @param value The new value for the lexeme Counter.
   * @apilevel high-level
   */
  public Eight_Queens setCounter(int value) {
    tokenint_Counter = value;
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      getCounter_handler.notifyDependencies();
    
    
    
    
    }
    return this;
  }
  /** @apilevel internal 
   */
  protected int tokenint_Counter;
  /**
   * Retrieves the value for the lexeme Counter.
   * @return The value for the lexeme Counter.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Counter")
  public int getCounter() {
    
    state().addHandlerDepTo(getCounter_handler);
    return tokenint_Counter;
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
