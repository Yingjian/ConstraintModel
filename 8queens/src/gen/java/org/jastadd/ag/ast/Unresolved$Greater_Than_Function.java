package org.jastadd.ag.ast;

import java.util.*;
/**
 * @ast class
 * @aspect RefResolverHelpers
 * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1557
 */
 class Unresolved$Greater_Than_Function extends Greater_Than_Function implements Unresolved$Node$Interface {
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1558
   */
  
    private String unresolved$Token;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1559
   */
  
    public String getUnresolved$Token() {
      return unresolved$Token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1562
   */
  
    void setUnresolved$Token(String token) {
      this.unresolved$Token = token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1565
   */
  
    private boolean unresolved$ResolveOpposite;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1566
   */
  
    public boolean getUnresolved$ResolveOpposite() {
      return unresolved$ResolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1569
   */
  
    void setUnresolved$ResolveOpposite(boolean resolveOpposite) {
      this.unresolved$ResolveOpposite = resolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1576
   */
  Unresolved$Node$Interface as$Unresolved() {
    return this;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1582
   */
  boolean is$Unresolved() {
    return true;
  }

}
