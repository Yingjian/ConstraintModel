package org.jastadd.ag.ast;

import java.util.*;
/**
 * @ast class
 * @aspect RefResolverHelpers
 * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1193
 */
abstract class Unresolved$Self_Function extends Self_Function implements Unresolved$Node$Interface {
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1194
   */
  
    private String unresolved$Token;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1195
   */
  
    public String getUnresolved$Token() {
      return unresolved$Token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1198
   */
  
    void setUnresolved$Token(String token) {
      this.unresolved$Token = token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1201
   */
  
    private boolean unresolved$ResolveOpposite;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1202
   */
  
    public boolean getUnresolved$ResolveOpposite() {
      return unresolved$ResolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1205
   */
  
    void setUnresolved$ResolveOpposite(boolean resolveOpposite) {
      this.unresolved$ResolveOpposite = resolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1212
   */
  Unresolved$Node$Interface as$Unresolved() {
    return this;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1218
   */
  boolean is$Unresolved() {
    return true;
  }

}
