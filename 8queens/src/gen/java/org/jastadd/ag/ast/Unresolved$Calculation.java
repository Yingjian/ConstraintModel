package org.jastadd.ag.ast;

import java.util.*;
/**
 * @ast class
 * @aspect RefResolverHelpers
 * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1221
 */
abstract class Unresolved$Calculation extends Calculation implements Unresolved$Node$Interface {
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1222
   */
  
    private String unresolved$Token;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1223
   */
  
    public String getUnresolved$Token() {
      return unresolved$Token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1226
   */
  
    void setUnresolved$Token(String token) {
      this.unresolved$Token = token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1229
   */
  
    private boolean unresolved$ResolveOpposite;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1230
   */
  
    public boolean getUnresolved$ResolveOpposite() {
      return unresolved$ResolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1233
   */
  
    void setUnresolved$ResolveOpposite(boolean resolveOpposite) {
      this.unresolved$ResolveOpposite = resolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1240
   */
  Unresolved$Node$Interface as$Unresolved() {
    return this;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1246
   */
  boolean is$Unresolved() {
    return true;
  }

}
