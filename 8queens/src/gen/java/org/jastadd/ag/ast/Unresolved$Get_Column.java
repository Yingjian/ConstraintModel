package org.jastadd.ag.ast;

import java.util.*;
/**
 * @ast class
 * @aspect RefResolverHelpers
 * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1753
 */
 class Unresolved$Get_Column extends Get_Column implements Unresolved$Node$Interface {
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1754
   */
  
    private String unresolved$Token;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1755
   */
  
    public String getUnresolved$Token() {
      return unresolved$Token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1758
   */
  
    void setUnresolved$Token(String token) {
      this.unresolved$Token = token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1761
   */
  
    private boolean unresolved$ResolveOpposite;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1762
   */
  
    public boolean getUnresolved$ResolveOpposite() {
      return unresolved$ResolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1765
   */
  
    void setUnresolved$ResolveOpposite(boolean resolveOpposite) {
      this.unresolved$ResolveOpposite = resolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1772
   */
  Unresolved$Node$Interface as$Unresolved() {
    return this;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\8queens\\src\\gen\\jastadd\\agRefResolver.jadd:1778
   */
  boolean is$Unresolved() {
    return true;
  }

}
