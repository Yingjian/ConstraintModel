/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\ag.ast:43
 * @astdecl Player : ASTNode ::= <Taken_Coins:int> <Proposal:int> <i:boolean>;
 * @production Player : {@link ASTNode} ::= <span class="component">&lt;Taken_Coins:{@link int}&gt;</span> <span class="component">&lt;Proposal:{@link int}&gt;</span> <span class="component">&lt;i:{@link boolean}&gt;</span>;

 */
public class Player extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:507
   */
  public static Player createRef(String ref) {
    Unresolved$Player unresolvedNode = new Unresolved$Player();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:513
   */
  public static Player createRefDirection(String ref) {
    Unresolved$Player unresolvedNode = new Unresolved$Player();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:710
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:1914
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:1920
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Player() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    state().enterConstruction();
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Taken_Coins", "Proposal", "i"},
    type = {"int", "int", "boolean"},
    kind = {"Token", "Token", "Token"}
  )
  public Player(int p0, int p1, boolean p2) {
state().enterConstruction();
    setTaken_Coins(p0);
    setProposal(p1);
    seti(p2);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:27
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:35
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  public Player clone() throws CloneNotSupportedException {
    Player node = (Player) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:50
   */
  public Player copy() {
    try {
      Player node = (Player) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:74
   */
  @Deprecated
  public Player fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:84
   */
  public Player treeCopyNoTransform() {
    Player tree = (Player) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:105
   */
  public Player treeCopy() {
    Player tree = (Player) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:120
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:123
   */
  protected void inc_copyHandlers(Player copy) {
    super.inc_copyHandlers(copy);

        if (getTaken_Coins_handler != null) {
          copy.getTaken_Coins_handler = ASTNode$DepGraphNode.createAstHandler(getTaken_Coins_handler, copy);
        }
        if (getProposal_handler != null) {
          copy.getProposal_handler = ASTNode$DepGraphNode.createAstHandler(getProposal_handler, copy);
        }
        if (geti_handler != null) {
          copy.geti_handler = ASTNode$DepGraphNode.createAstHandler(geti_handler, copy);
        }
  }
  /** @apilevel internal 
   * @declaredat ASTNode:138
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:145
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:147
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  if (getTaken_Coins_handler != null) {
    getTaken_Coins_handler.throwAway();
  }
  if (getProposal_handler != null) {
    getProposal_handler.throwAway();
  }
  if (geti_handler != null) {
    geti_handler.throwAway();
  }
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:165
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:166
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  if (getTaken_Coins_handler != null) {
    getTaken_Coins_handler.cleanupListeners();
  }
  if (getProposal_handler != null) {
    getProposal_handler.cleanupListeners();
  }
  if (geti_handler != null) {
    geti_handler.cleanupListeners();
  }
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:183
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:184
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   */
  protected ASTNode$DepGraphNode getTaken_Coins_handler = ASTNode$DepGraphNode.createAstHandler(this, "getTaken_Coins", null);
  /**
   * Replaces the lexeme Taken_Coins.
   * @param value The new value for the lexeme Taken_Coins.
   * @apilevel high-level
   */
  public Player setTaken_Coins(int value) {
    tokenint_Taken_Coins = value;
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      getTaken_Coins_handler.notifyDependencies();
    
    
    
    
    }
    return this;
  }
  /** @apilevel internal 
   */
  protected int tokenint_Taken_Coins;
  /**
   * Retrieves the value for the lexeme Taken_Coins.
   * @return The value for the lexeme Taken_Coins.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Taken_Coins")
  public int getTaken_Coins() {
    
    state().addHandlerDepTo(getTaken_Coins_handler);
    return tokenint_Taken_Coins;
  }
  /**
   */
  protected ASTNode$DepGraphNode getProposal_handler = ASTNode$DepGraphNode.createAstHandler(this, "getProposal", null);
  /**
   * Replaces the lexeme Proposal.
   * @param value The new value for the lexeme Proposal.
   * @apilevel high-level
   */
  public Player setProposal(int value) {
    tokenint_Proposal = value;
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      getProposal_handler.notifyDependencies();
    
    
    
    
    }
    return this;
  }
  /** @apilevel internal 
   */
  protected int tokenint_Proposal;
  /**
   * Retrieves the value for the lexeme Proposal.
   * @return The value for the lexeme Proposal.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Proposal")
  public int getProposal() {
    
    state().addHandlerDepTo(getProposal_handler);
    return tokenint_Proposal;
  }
  /**
   */
  protected ASTNode$DepGraphNode geti_handler = ASTNode$DepGraphNode.createAstHandler(this, "geti", null);
  /**
   * Replaces the lexeme i.
   * @param value The new value for the lexeme i.
   * @apilevel high-level
   */
  public Player seti(boolean value) {
    tokenboolean_i = value;
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      geti_handler.notifyDependencies();
    
    
    
    
    }
    return this;
  }
  /** @apilevel internal 
   */
  protected boolean tokenboolean_i;
  /**
   * Retrieves the value for the lexeme i.
   * @return The value for the lexeme i.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="i")
  public boolean geti() {
    
    state().addHandlerDepTo(geti_handler);
    return tokenboolean_i;
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
