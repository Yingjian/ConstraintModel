package org.jastadd.ag.ast;

import java.util.*;
/**
 * @ast class
 * @aspect RefResolverHelpers
 * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:1030
 */
abstract class Unresolved$Binary_Relation extends Binary_Relation implements Unresolved$Node$Interface {
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:1031
   */
  
    private String unresolved$Token;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:1032
   */
  
    public String getUnresolved$Token() {
      return unresolved$Token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:1035
   */
  
    void setUnresolved$Token(String token) {
      this.unresolved$Token = token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:1038
   */
  
    private boolean unresolved$ResolveOpposite;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:1039
   */
  
    public boolean getUnresolved$ResolveOpposite() {
      return unresolved$ResolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:1042
   */
  
    void setUnresolved$ResolveOpposite(boolean resolveOpposite) {
      this.unresolved$ResolveOpposite = resolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:1049
   */
  Unresolved$Node$Interface as$Unresolved() {
    return this;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:1055
   */
  boolean is$Unresolved() {
    return true;
  }

}
