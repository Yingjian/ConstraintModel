package org.jastadd.ag.ast;

import java.util.*;
/** @apilevel internal 
 * @ast class
 * @declaredat ASTNode:2
 */
public class ASTNode$DepGraphNode extends java.lang.Object {
  /**
   * @declaredat ASTNode:6
   */
  

  // Level: param

  public ASTNode fNode;
  /**
   * @declaredat ASTNode:7
   */
  
  public String fAttrID;
  /**
   * @declaredat ASTNode:8
   */
  
  protected Object fParams;
  /**
   * @declaredat ASTNode:10
   */
  

  public static ASTNode$DepGraphNode createAttrHandler(ASTNode node, String attrID, Object params) {
    return new ASTNode$DepGraphNode(node, attrID, params, ASTNode.inc_EMPTY);
  }
  /**
   * @declaredat ASTNode:14
   */
  

  public static ASTNode$DepGraphNode createAttrHandler(ASTNode$DepGraphNode handler, ASTNode node) {
    return new ASTNode$DepGraphNode(handler, node, ASTNode.inc_EMPTY);
  }
  /**
   * @declaredat ASTNode:18
   */
  

  public static ASTNode$DepGraphNode createAstHandler(ASTNode node, String attrID, Object params) {
    return new ASTNode$DepGraphNode(node, attrID, params, ASTNode.inc_AST);
  }
  /**
   * @declaredat ASTNode:22
   */
  

  public static ASTNode$DepGraphNode createAstHandler(ASTNode$DepGraphNode handler, ASTNode node) {
    return new ASTNode$DepGraphNode(handler, node, ASTNode.inc_AST);
  }
  /**
   * @declaredat ASTNode:27
   */
  


  ASTNode$DepGraphNode(ASTNode node, String attrID, Object params, int state) {
    fNode = node;
    fAttrID = attrID;
    fParams = params;
    fState = state;
  }
  /**
   * @declaredat ASTNode:34
   */
  

  ASTNode$DepGraphNode(ASTNode$DepGraphNode handler, ASTNode node, int state) {
    fNode = node;
    fAttrID = handler.fAttrID;
    fParams = handler.fParams;
    fState = state;
  }
  /**
   * @declaredat ASTNode:41
   */
  

  public void setParams(Object params) {
    fParams = params;
  }
  /**
   * @declaredat ASTNode:52
   */
  






  // Dependency management

  public java.util.HashSet<ASTNode$DepGraphNode> fListenerSet = new java.util.HashSet<ASTNode$DepGraphNode>(4);
  /**
   * @declaredat ASTNode:54
   */
  

  public boolean hasListeners() {
    return !fListenerSet.isEmpty();
  }
  /**
   * @declaredat ASTNode:58
   */
  

  public void addListener(ASTNode$DepGraphNode node) {
    fListenerSet.add(node);
  }
  /**
   * @declaredat ASTNode:62
   */
  

  public void removeListener(ASTNode$DepGraphNode node) {
    fListenerSet.remove(node);
  }
  /**
   * @declaredat ASTNode:66
   */
  

  public void cleanupListeners() {
    java.util.Iterator<ASTNode$DepGraphNode> itr = fListenerSet.iterator();
    while (itr.hasNext()) {
      ASTNode$DepGraphNode node = itr.next();
      if (node.isEmpty() || node.isGarbage()) {
        itr.remove();
      }
    }
  }
  /**
   * @declaredat ASTNode:76
   */
  

  public void clearListeners() {
    fListenerSet.clear();
  }
  /**
   * @declaredat ASTNode:82
   */
  

  // Notification

  private boolean visited = false;
  /**
   * @declaredat ASTNode:84
   */
  

  public void notifyDependencies() {
    // Notify and remove listeners
    if (!visited) {
      visited = true;
      java.util.HashSet<ASTNode$DepGraphNode> k = fListenerSet;
      fListenerSet = new java.util.HashSet<ASTNode$DepGraphNode>(4);
      for (ASTNode$DepGraphNode node : k) {
        if (!node.isGarbage()) {
          node.dependencyChanged();
        }
      }
      visited = false;
    }
  }
  /**
   * @declaredat ASTNode:101
   */
  

  // React to change

  public void dependencyChanged() {
    if (isComputed() || isCreated()) {
      setEmpty();
      reactToDependencyChange();
    }
  }
  /**
   * @declaredat ASTNode:108
   */
  

  public void reactToDependencyChange() {
    fNode.reactToDependencyChange(fAttrID, fParams);
  }
  /**
   * @declaredat ASTNode:114
   */
  

  // State

  protected int fState = ASTNode.inc_EMPTY;
  /**
   * @declaredat ASTNode:116
   */
  

  public void throwAway() {
    fState = ASTNode.inc_GARBAGE;
  }
  /**
   * @declaredat ASTNode:120
   */
  

  public void keepAlive() {
    fState = ASTNode.inc_LIVE;
  }
  /**
   * @declaredat ASTNode:124
   */
  

  public void setComputed() {
    fState = ASTNode.inc_COMPUTED;
  }
  /**
   * @declaredat ASTNode:128
   */
  

  public void setEmpty() {
    fState = ASTNode.inc_EMPTY;
  }
  /**
   * @declaredat ASTNode:132
   */
  

  public boolean isGarbage() {
    return fState == ASTNode.inc_GARBAGE;
  }
  /**
   * @declaredat ASTNode:136
   */
  

  public boolean isCreated() {
    return fState == ASTNode.inc_CREATED;
  }
  /**
   * @declaredat ASTNode:140
   */
  

  public boolean isCloned() {
    return fState == ASTNode.inc_CLONED;
  }
  /**
   * @declaredat ASTNode:144
   */
  

  public boolean isLive() {
    return fState == ASTNode.inc_LIVE;
  }
  /**
   * @declaredat ASTNode:148
   */
  

  public boolean isComputed() {
    return fState == ASTNode.inc_COMPUTED;
  }
  /**
   * @declaredat ASTNode:152
   */
  

  public boolean isEmpty() {
    return fState == ASTNode.inc_EMPTY;
  }
  /**
   * @declaredat ASTNode:159
   */
  


  // Clean up

  public boolean visitedDuringCleanup = false;
  /**
   * @declaredat ASTNode:160
   */
  
  public static int nbr_cleanup = 0;
  /**
   * @declaredat ASTNode:162
   */
  

  public void cleanUpGarbage() {
    visitedDuringCleanup = true;
    nbr_cleanup++;
    // Clean up garbage
    java.util.Iterator<ASTNode$DepGraphNode> itr = fListenerSet.iterator();
    while (itr.hasNext()) {
      ASTNode$DepGraphNode cur = itr.next();
      if (cur.isGarbage()) {
        itr.remove();
      }
    }
  }

}
