/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\ag.ast:42
 * @astdecl Take_Away_Game : ASTNode ::= Player_1:Player Player_2:Player <Coins:int> <Turn:int> <P:boolean>;
 * @production Take_Away_Game : {@link ASTNode} ::= <span class="component">Player_1:{@link Player}</span> <span class="component">Player_2:{@link Player}</span> <span class="component">&lt;Coins:{@link int}&gt;</span> <span class="component">&lt;Turn:{@link int}&gt;</span> <span class="component">&lt;P:{@link boolean}&gt;</span>;

 */
public class Take_Away_Game extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect init
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\main\\jastadd\\TakeAway\\init.jadd:2
   */
  public void init(int i){
        this.setCoins(i);
        this.setTurn(0);
        Player Player1 = new Player();
        Player Player2 = new Player();
        Player1.setTaken_Coins(0);
        Player2.setTaken_Coins(0);
        Player1.setProposal(0);
        Player2.setProposal(0);
        this.setPlayer_1(Player1);
        this.setPlayer_2(Player2);
    }
  /**
   * @aspect init
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\main\\jastadd\\TakeAway\\init.jadd:14
   */
  public boolean end(){
        Constraint_Model cm =new Constraint_Model();
        return cm.End_Game_Checker(this);
    }
  /**
   * @aspect init
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\main\\jastadd\\TakeAway\\init.jadd:18
   */
  public void move(){
        Constraint_Model cm= new Constraint_Model();
        if(cm.Turn_Parity(this)){
            System.out.println("Player1's turn to take coins:");
            int amountOfCoins = 0;
            Scanner sc=new Scanner(System.in);
	    while(!cm.Valid_Move_Checker(this, this.getPlayer_1())){//input #disks
	      String s=sc.next();
	      amountOfCoins = Integer.parseInt(s); 
          System.out.println("Player1 took " + amountOfCoins);
          this.setCoins(this.getCoins()-amountOfCoins);
	    }
	    sc.close();
        this.getPlayer_1().setTaken_Coins(this.getPlayer_1().getTaken_Coins()+ amountOfCoins);
        }else{
            System.out.println("Player2's turn to take coins:");
        int amountOfCoins = 0;
        Scanner sc=new Scanner(System.in);
        while(!cm.Valid_Move_Checker(this, this.getPlayer_2())){//input #disks
        String s=sc.next();
        amountOfCoins = Integer.parseInt(s); 
        System.out.println("Player2 took " + amountOfCoins);
    }
        sc.close();
        this.getPlayer_2().setTaken_Coins(this.getPlayer_2().getTaken_Coins()+ amountOfCoins);
        this.setCoins(this.getCoins()-amountOfCoins);
    }
        }
  /**
   * @aspect in_game_constraints
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\main\\jastadd\\TakeAway\\In_Game_Constraints.jadd:40
   */
  int Turn(int T){
          return T+1;
      }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:495
   */
  public static Take_Away_Game createRef(String ref) {
    Unresolved$Take_Away_Game unresolvedNode = new Unresolved$Take_Away_Game();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:501
   */
  public static Take_Away_Game createRefDirection(String ref) {
    Unresolved$Take_Away_Game unresolvedNode = new Unresolved$Take_Away_Game();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:706
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:1886
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:1892
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Take_Away_Game() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[2];  getChild_handler = new ASTNode$DepGraphNode[children.length];
    state().enterConstruction();
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:15
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Player_1", "Player_2", "Coins", "Turn", "P"},
    type = {"Player", "Player", "int", "int", "boolean"},
    kind = {"Child", "Child", "Token", "Token", "Token"}
  )
  public Take_Away_Game(Player p0, Player p1, int p2, int p3, boolean p4) {
state().enterConstruction();
    setChild(p0, 0);
    setChild(p1, 1);
    setCoins(p2);
    setTurn(p3);
    setP(p4);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:30
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:38
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:48
   */
  public Take_Away_Game clone() throws CloneNotSupportedException {
    Take_Away_Game node = (Take_Away_Game) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:53
   */
  public Take_Away_Game copy() {
    try {
      Take_Away_Game node = (Take_Away_Game) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:77
   */
  @Deprecated
  public Take_Away_Game fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:87
   */
  public Take_Away_Game treeCopyNoTransform() {
    Take_Away_Game tree = (Take_Away_Game) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:108
   */
  public Take_Away_Game treeCopy() {
    Take_Away_Game tree = (Take_Away_Game) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:123
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:126
   */
  protected void inc_copyHandlers(Take_Away_Game copy) {
    super.inc_copyHandlers(copy);

        if (getCoins_handler != null) {
          copy.getCoins_handler = ASTNode$DepGraphNode.createAstHandler(getCoins_handler, copy);
        }
        if (getTurn_handler != null) {
          copy.getTurn_handler = ASTNode$DepGraphNode.createAstHandler(getTurn_handler, copy);
        }
        if (getP_handler != null) {
          copy.getP_handler = ASTNode$DepGraphNode.createAstHandler(getP_handler, copy);
        }
  }
  /** @apilevel internal 
   * @declaredat ASTNode:141
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:148
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:150
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  if (getCoins_handler != null) {
    getCoins_handler.throwAway();
  }
  if (getTurn_handler != null) {
    getTurn_handler.throwAway();
  }
  if (getP_handler != null) {
    getP_handler.throwAway();
  }
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:168
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:169
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  if (getCoins_handler != null) {
    getCoins_handler.cleanupListeners();
  }
  if (getTurn_handler != null) {
    getTurn_handler.cleanupListeners();
  }
  if (getP_handler != null) {
    getP_handler.cleanupListeners();
  }
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:186
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:187
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   * Replaces the Player_1 child.
   * @param node The new node to replace the Player_1 child.
   * @apilevel high-level
   */
  public Take_Away_Game setPlayer_1(Player node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the Player_1 child.
   * @return The current node used as the Player_1 child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Player_1")
  public Player getPlayer_1() {
    return (Player) getChild(0);
  }
  /**
   * Retrieves the Player_1 child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Player_1 child.
   * @apilevel low-level
   */
  public Player getPlayer_1NoTransform() {
    return (Player) getChildNoTransform(0);
  }
  /**
   * Replaces the Player_2 child.
   * @param node The new node to replace the Player_2 child.
   * @apilevel high-level
   */
  public Take_Away_Game setPlayer_2(Player node) {
    setChild(node, 1);
    return this;
  }
  /**
   * Retrieves the Player_2 child.
   * @return The current node used as the Player_2 child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Player_2")
  public Player getPlayer_2() {
    return (Player) getChild(1);
  }
  /**
   * Retrieves the Player_2 child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Player_2 child.
   * @apilevel low-level
   */
  public Player getPlayer_2NoTransform() {
    return (Player) getChildNoTransform(1);
  }
  /**
   */
  protected ASTNode$DepGraphNode getCoins_handler = ASTNode$DepGraphNode.createAstHandler(this, "getCoins", null);
  /**
   * Replaces the lexeme Coins.
   * @param value The new value for the lexeme Coins.
   * @apilevel high-level
   */
  public Take_Away_Game setCoins(int value) {
    tokenint_Coins = value;
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      getCoins_handler.notifyDependencies();
    
    
    
    
    }
    return this;
  }
  /** @apilevel internal 
   */
  protected int tokenint_Coins;
  /**
   * Retrieves the value for the lexeme Coins.
   * @return The value for the lexeme Coins.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Coins")
  public int getCoins() {
    
    state().addHandlerDepTo(getCoins_handler);
    return tokenint_Coins;
  }
  /**
   */
  protected ASTNode$DepGraphNode getTurn_handler = ASTNode$DepGraphNode.createAstHandler(this, "getTurn", null);
  /**
   * Replaces the lexeme Turn.
   * @param value The new value for the lexeme Turn.
   * @apilevel high-level
   */
  public Take_Away_Game setTurn(int value) {
    tokenint_Turn = value;
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      getTurn_handler.notifyDependencies();
    
    
    
    
    }
    return this;
  }
  /** @apilevel internal 
   */
  protected int tokenint_Turn;
  /**
   * Retrieves the value for the lexeme Turn.
   * @return The value for the lexeme Turn.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Turn")
  public int getTurn() {
    
    state().addHandlerDepTo(getTurn_handler);
    return tokenint_Turn;
  }
  /**
   */
  protected ASTNode$DepGraphNode getP_handler = ASTNode$DepGraphNode.createAstHandler(this, "getP", null);
  /**
   * Replaces the lexeme P.
   * @param value The new value for the lexeme P.
   * @apilevel high-level
   */
  public Take_Away_Game setP(boolean value) {
    tokenboolean_P = value;
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      getP_handler.notifyDependencies();
    
    
    
    
    }
    return this;
  }
  /** @apilevel internal 
   */
  protected boolean tokenboolean_P;
  /**
   * Retrieves the value for the lexeme P.
   * @return The value for the lexeme P.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="P")
  public boolean getP() {
    
    state().addHandlerDepTo(getP_handler);
    return tokenboolean_P;
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
