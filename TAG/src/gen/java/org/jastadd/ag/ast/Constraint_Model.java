/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\ag.ast:1
 * @astdecl Constraint_Model : ASTNode ::= Formula*;
 * @production Constraint_Model : {@link ASTNode} ::= <span class="component">{@link Formula}*</span>;

 */
public class Constraint_Model extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect in_game_constraints
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\main\\jastadd\\TakeAway\\In_Game_Constraints.jadd:2
   */
  public boolean Turn_Parity(Take_Away_Game G){
        Constant_Num Num1         = new Constant_Num(1);
        Constant_Num Num2         = new Constant_Num(2);
        Turn Current_Turn = new Turn(G);
        Mod Parity               = new Mod(Current_Turn, Num2);
        Equal Parity_is_Odd      = new Equal(Parity, Num1);
        Atom Odd_Turn            = new Atom(Parity_is_Odd);
        this.addFormula(Odd_Turn);
        return this.getFormula(this.getNumFormula()-1).evaluation();
      }
  /**
   * @aspect in_game_constraints
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\main\\jastadd\\TakeAway\\In_Game_Constraints.jadd:12
   */
  public boolean Valid_Move_Checker(Take_Away_Game G, Player P){
        Constant_Num Num1            = new Constant_Num(1);
        Constant_Num Num3            = new Constant_Num(3);
        Coins_on_Table Remaining_Coins = new Coins_on_Table(G);
        Smaller_Than_Function Upper_Bound =new Smaller_Than_Function(Remaining_Coins, Num3);
        IfThenElse Max_Proposal         = new IfThenElse(Upper_Bound, Remaining_Coins, Num3);
        Player_Proposal Proposed_Number = new Player_Proposal(P);
        //System.out.println("Player_Proposal"+Proposed_Number.evaluation());
        //P.setProposal(3);
        //System.out.println("Player_Proposal"+Proposed_Number.evaluation());
        Greater_Equal Proposal_Meets_Lower_Bound = new Greater_Equal(Proposed_Number, Num1);
        Smaller_Equal Proposal_Meets_Upper_Bound = new Smaller_Equal(Proposed_Number, Max_Proposal);
        Atom Valid_Subcase_1 = new Atom(Proposal_Meets_Lower_Bound);
        //System.out.println("Player_Proposal"+Proposed_Number.evaluation() +" "+ Valid_Subcase_1.evaluation());
        Atom Valid_Subcase_2 = new Atom(Proposal_Meets_Upper_Bound);
        //System.out.println("MAX"+Max_Proposal.evaluation() +" "+Valid_Subcase_2.evaluation());
        Conjunction Valid_Proposal = new Conjunction(Valid_Subcase_1, Valid_Subcase_2);
        this.addFormula(Valid_Proposal);
        return this.getFormula(this.getNumFormula()-1).evaluation();
      }
  /**
   * @aspect in_game_constraints
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\main\\jastadd\\TakeAway\\In_Game_Constraints.jadd:32
   */
  public boolean End_Game_Checker(Take_Away_Game G){
        Constant_Num Num0              = new Constant_Num(0);
        Constant_Num Remaining_Coins   = new Constant_Num(G.getCoins());
        Equal Remaining_Coins_Equal_0 = new Equal(Remaining_Coins, Num0);
        Atom End_of_Game              = new Atom(Remaining_Coins_Equal_0);
        this.addFormula(End_of_Game);
        return this.getFormula(this.getNumFormula()-1).evaluation();
      }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:3
   */
  public static Constraint_Model createRef(String ref) {
    Unresolved$Constraint_Model unresolvedNode = new Unresolved$Constraint_Model();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:9
   */
  public static Constraint_Model createRefDirection(String ref) {
    Unresolved$Constraint_Model unresolvedNode = new Unresolved$Constraint_Model();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:540
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:738
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:744
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Constraint_Model() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[1];  getChild_handler = new ASTNode$DepGraphNode[children.length];
    state().enterConstruction();
    setChild(new JastAddList(), 0);
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:16
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Formula"},
    type = {"JastAddList<Formula>"},
    kind = {"List"}
  )
  public Constraint_Model(JastAddList<Formula> p0) {
state().enterConstruction();
    setChild(p0, 0);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:27
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:35
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  public Constraint_Model clone() throws CloneNotSupportedException {
    Constraint_Model node = (Constraint_Model) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:50
   */
  public Constraint_Model copy() {
    try {
      Constraint_Model node = (Constraint_Model) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:74
   */
  @Deprecated
  public Constraint_Model fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:84
   */
  public Constraint_Model treeCopyNoTransform() {
    Constraint_Model tree = (Constraint_Model) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:105
   */
  public Constraint_Model treeCopy() {
    Constraint_Model tree = (Constraint_Model) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:120
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:123
   */
  protected void inc_copyHandlers(Constraint_Model copy) {
    super.inc_copyHandlers(copy);

  }
  /** @apilevel internal 
   * @declaredat ASTNode:129
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:136
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:138
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:147
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:148
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:156
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:157
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   * Replaces the Formula list.
   * @param list The new list node to be used as the Formula list.
   * @apilevel high-level
   */
  public Constraint_Model setFormulaList(JastAddList<Formula> list) {
    setChild(list, 0);
    return this;
  }
  /**
   * Retrieves the number of children in the Formula list.
   * @return Number of children in the Formula list.
   * @apilevel high-level
   */
  public int getNumFormula() {
    return getFormulaList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Formula list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Formula list.
   * @apilevel low-level
   */
  public int getNumFormulaNoTransform() {
    return getFormulaListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Formula list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Formula list.
   * @apilevel high-level
   */
  public Formula getFormula(int i) {
    return (Formula) getFormulaList().getChild(i);
  }
  /**
   * Check whether the Formula list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasFormula() {
    return getFormulaList().getNumChild() != 0;
  }
  /**
   * Append an element to the Formula list.
   * @param node The element to append to the Formula list.
   * @apilevel high-level
   */
  public Constraint_Model addFormula(Formula node) {
    JastAddList<Formula> list = (parent == null) ? getFormulaListNoTransform() : getFormulaList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public Constraint_Model addFormulaNoTransform(Formula node) {
    JastAddList<Formula> list = getFormulaListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the Formula list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public Constraint_Model setFormula(Formula node, int i) {
    JastAddList<Formula> list = getFormulaList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the Formula list.
   * @return The node representing the Formula list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Formula")
  public JastAddList<Formula> getFormulaList() {
    JastAddList<Formula> list = (JastAddList<Formula>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Formula list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Formula list.
   * @apilevel low-level
   */
  public JastAddList<Formula> getFormulaListNoTransform() {
    return (JastAddList<Formula>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Formula list without
   * triggering rewrites.
   */
  public Formula getFormulaNoTransform(int i) {
    return (Formula) getFormulaListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Formula list.
   * @return The node representing the Formula list.
   * @apilevel high-level
   */
  public JastAddList<Formula> getFormulas() {
    return getFormulaList();
  }
  /**
   * Retrieves the Formula list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Formula list.
   * @apilevel low-level
   */
  public JastAddList<Formula> getFormulasNoTransform() {
    return getFormulaListNoTransform();
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
