package org.jastadd.ag.ast;

import java.util.*;

/**
 * @ast interface
 * @aspect RefResolverHelpers
 * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:717
 */
 interface Unresolved$Node$Interface {

     
    String getUnresolved$Token();

     
    boolean getUnresolved$ResolveOpposite();
}
