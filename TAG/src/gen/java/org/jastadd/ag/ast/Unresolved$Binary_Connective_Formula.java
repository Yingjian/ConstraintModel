package org.jastadd.ag.ast;

import java.util.*;
/**
 * @ast class
 * @aspect RefResolverHelpers
 * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:806
 */
abstract class Unresolved$Binary_Connective_Formula extends Binary_Connective_Formula implements Unresolved$Node$Interface {
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:807
   */
  
    private String unresolved$Token;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:808
   */
  
    public String getUnresolved$Token() {
      return unresolved$Token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:811
   */
  
    void setUnresolved$Token(String token) {
      this.unresolved$Token = token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:814
   */
  
    private boolean unresolved$ResolveOpposite;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:815
   */
  
    public boolean getUnresolved$ResolveOpposite() {
      return unresolved$ResolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:818
   */
  
    void setUnresolved$ResolveOpposite(boolean resolveOpposite) {
      this.unresolved$ResolveOpposite = resolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:825
   */
  Unresolved$Node$Interface as$Unresolved() {
    return this;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:831
   */
  boolean is$Unresolved() {
    return true;
  }

}
