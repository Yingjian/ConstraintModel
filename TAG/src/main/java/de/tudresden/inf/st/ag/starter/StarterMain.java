package de.tudresden.inf.st.ag.starter;
import org.jastadd.ag.ast.*;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.nio.charset.StandardCharsets;

public class StarterMain {
  public static void main(String[] args){
	  System.out.println("Hello, please input amount of Coins:");
	  int amountOfCoins = 0;
      Scanner sc=new Scanner(System.in);
	    while(amountOfCoins <= 0){//input #disks
	      String s=sc.next();
	      amountOfCoins = checkInt(s); 
	    }
		//public boolean Pillar.moveTo(Pillar P){
      //int i = this.getNumDisk();
     // P.addDisk(this.getDisk(i-1));
     // this.getDisks().removeChild(i-1);
     // return true;
//  }
	    //sc.close();
	    game(amountOfCoins);
    }
	public static void game(int i) {	    
	    Take_Away_Game TAG = new Take_Away_Game();
	    TAG.init(i);
		//Player Player1 = new Player();
        //Player1.setTaken_Coins(7);
        //Player1.setProposal(9);
        //TAG.setP(Player1);
		//System.out.println("hasP:" +TAG.hasP());
		//System.out.println("hasP:" +TAG.getP().getProposal());
		//TAG.getPOpt().removeChild(0);
		//System.out.println("hasP:" +TAG.hasP());
		//TAG.getPlayer_1().setProposal(3);
		//TAG.setP(Player1);
		//System.out.println("hasP:" +TAG.hasP());
		//System.out.println("hasP:" +TAG.getP().getProposal());
		//System.out.println("Player2's turnasdadfasfasf:" +TAG.getPlayer_1().getProposal());
		while(!TAG.end()){
			Constraint_Model cm= new Constraint_Model();
			TAG.setTurn(TAG.getTurn() + 1);
			TAG.setP(cm.Turn_Parity(TAG));
			System.out.println("Turn:"+TAG.getTurn());
			if(TAG.getP()){
            	System.out.println("Player1's turn to take coins:");
			//int Coins = 0;
				Scanner sc=new Scanner(System.in);
			//while(!){//input #disks
				String s=sc.next();
				int temp = checkInt(s);
	       		TAG.getPlayer_2().setProposal(temp);
				if(cm.Valid_Move_Checker(TAG, TAG.getPlayer_2())){
					TAG.setCoins(TAG.getCoins()-TAG.getPlayer_2().getProposal());
					TAG.getPlayer_2().setProposal(0);
				}else{
					TAG.setTurn(TAG.getTurn() - 1);
					System.out.println("Player1's proposal is not valid.");
				}
	    	//sc.close();
			}else{
				System.out.println("Player2's turn to take coins:");
			//int Coins = 0;
				Scanner sc=new Scanner(System.in);
				String s=sc.next();
				int temp = checkInt(s);
	       		TAG.getPlayer_1().setProposal(temp);
				//System.out.println("Player1's turnasdadfasfasf:" +TAG.getPlayer_1().getProposal());
				if(cm.Valid_Move_Checker(TAG, TAG.getPlayer_1())){
					TAG.setCoins(TAG.getCoins()-TAG.getPlayer_1().getProposal());
					TAG.getPlayer_1().setProposal(0);
				}else{
					TAG.setTurn(TAG.getTurn() - 1);
					System.out.println("Player2's proposal is not valid.");
				}
			}System.out.println("Remaining coins:" +TAG.getCoins());
			
		}if(TAG.getP()){
			System.out.println("Player2 wins.");
		}
		else{
			System.out.println("Player1 wins.");
		}
	    System.out.println("Game done.");
	    
  }
      public static int checkInt(String str){
        int a =0;
        try {
          a = Integer.parseInt(str);
          } catch (NumberFormatException e) {
            return 0;
          }
        return a;
      }
}