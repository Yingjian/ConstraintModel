package org.jastadd.ag.ast;

import java.util.*;
/**
 * @ast class
 * @aspect RefResolverHelpers
 * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1041
 */
 class Unresolved$IfThenElse extends IfThenElse implements Unresolved$Node$Interface {
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1042
   */
  
    private String unresolved$Token;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1043
   */
  
    public String getUnresolved$Token() {
      return unresolved$Token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1046
   */
  
    void setUnresolved$Token(String token) {
      this.unresolved$Token = token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1049
   */
  
    private boolean unresolved$ResolveOpposite;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1050
   */
  
    public boolean getUnresolved$ResolveOpposite() {
      return unresolved$ResolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1053
   */
  
    void setUnresolved$ResolveOpposite(boolean resolveOpposite) {
      this.unresolved$ResolveOpposite = resolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1060
   */
  Unresolved$Node$Interface as$Unresolved() {
    return this;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1066
   */
  boolean is$Unresolved() {
    return true;
  }

}
