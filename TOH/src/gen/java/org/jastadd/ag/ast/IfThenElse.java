/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\ag.ast:13
 * @astdecl IfThenElse : Term ::= If:Condition Then:Term Else:Term;
 * @production IfThenElse : {@link Term} ::= <span class="component">If:{@link Condition}</span> <span class="component">Then:{@link Term}</span> <span class="component">Else:{@link Term}</span>;

 */
public class IfThenElse extends Term implements Cloneable {
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:147
   */
  public static IfThenElse createRef(String ref) {
    Unresolved$IfThenElse unresolvedNode = new Unresolved$IfThenElse();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:153
   */
  public static IfThenElse createRefDirection(String ref) {
    Unresolved$IfThenElse unresolvedNode = new Unresolved$IfThenElse();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:576
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1057
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1063
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public IfThenElse() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[3];  getChild_handler = new ASTNode$DepGraphNode[children.length];
    state().enterConstruction();
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:15
   */
  @ASTNodeAnnotation.Constructor(
    name = {"If", "Then", "Else"},
    type = {"Condition", "Term", "Term"},
    kind = {"Child", "Child", "Child"}
  )
  public IfThenElse(Condition p0, Term p1, Term p2) {
state().enterConstruction();
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:28
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 3;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:36
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:46
   */
  public IfThenElse clone() throws CloneNotSupportedException {
    IfThenElse node = (IfThenElse) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:51
   */
  public IfThenElse copy() {
    try {
      IfThenElse node = (IfThenElse) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:75
   */
  @Deprecated
  public IfThenElse fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:85
   */
  public IfThenElse treeCopyNoTransform() {
    IfThenElse tree = (IfThenElse) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:106
   */
  public IfThenElse treeCopy() {
    IfThenElse tree = (IfThenElse) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:121
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:124
   */
  protected void inc_copyHandlers(IfThenElse copy) {
    super.inc_copyHandlers(copy);

  }
  /** @apilevel internal 
   * @declaredat ASTNode:130
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:137
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:139
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:148
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:149
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:157
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:158
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   * Replaces the If child.
   * @param node The new node to replace the If child.
   * @apilevel high-level
   */
  public IfThenElse setIf(Condition node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the If child.
   * @return The current node used as the If child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="If")
  public Condition getIf() {
    return (Condition) getChild(0);
  }
  /**
   * Retrieves the If child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the If child.
   * @apilevel low-level
   */
  public Condition getIfNoTransform() {
    return (Condition) getChildNoTransform(0);
  }
  /**
   * Replaces the Then child.
   * @param node The new node to replace the Then child.
   * @apilevel high-level
   */
  public IfThenElse setThen(Term node) {
    setChild(node, 1);
    return this;
  }
  /**
   * Retrieves the Then child.
   * @return The current node used as the Then child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Then")
  public Term getThen() {
    return (Term) getChild(1);
  }
  /**
   * Retrieves the Then child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Then child.
   * @apilevel low-level
   */
  public Term getThenNoTransform() {
    return (Term) getChildNoTransform(1);
  }
  /**
   * Replaces the Else child.
   * @param node The new node to replace the Else child.
   * @apilevel high-level
   */
  public IfThenElse setElse(Term node) {
    setChild(node, 2);
    return this;
  }
  /**
   * Retrieves the Else child.
   * @return The current node used as the Else child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="Else")
  public Term getElse() {
    return (Term) getChild(2);
  }
  /**
   * Retrieves the Else child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the Else child.
   * @apilevel low-level
   */
  public Term getElseNoTransform() {
    return (Term) getChildNoTransform(2);
  }
  /**
   * @attribute syn
   * @aspect Connectives
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\main\\jastadd\\hanoi\\ConstraintsEvalforDouble.jrag:38
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Connectives", declaredAt="E:\\project\\ConstraintModel\\TOH\\src\\main\\jastadd\\hanoi\\ConstraintsEvalforDouble.jrag:38")
  public double evalDouble() {
    {
            if(this.getIf().SatisfiedDouble()){
               return this.getThen().evalDouble();
            }
            return this.getElse().evalDouble();
        }
  }
  /**
   * @attribute syn
   * @aspect Connectives
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\main\\jastadd\\hanoi\\Term.jrag:4
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Connectives", declaredAt="E:\\project\\ConstraintModel\\TOH\\src\\main\\jastadd\\hanoi\\Term.jrag:4")
  public int eval() {
    {
            if(this.getIf().Satisfied()){
               return this.getThen().eval();
            }
            return this.getElse().eval();
        }
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
