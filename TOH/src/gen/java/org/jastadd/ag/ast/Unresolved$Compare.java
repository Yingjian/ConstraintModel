package org.jastadd.ag.ast;

import java.util.*;
/**
 * @ast class
 * @aspect RefResolverHelpers
 * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1293
 */
 class Unresolved$Compare extends Compare implements Unresolved$Node$Interface {
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1294
   */
  
    private String unresolved$Token;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1295
   */
  
    public String getUnresolved$Token() {
      return unresolved$Token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1298
   */
  
    void setUnresolved$Token(String token) {
      this.unresolved$Token = token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1301
   */
  
    private boolean unresolved$ResolveOpposite;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1302
   */
  
    public boolean getUnresolved$ResolveOpposite() {
      return unresolved$ResolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1305
   */
  
    void setUnresolved$ResolveOpposite(boolean resolveOpposite) {
      this.unresolved$ResolveOpposite = resolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1312
   */
  Unresolved$Node$Interface as$Unresolved() {
    return this;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:1318
   */
  boolean is$Unresolved() {
    return true;
  }

}
