package org.jastadd.ag.ast;

import java.util.*;

/**
 * @ast interface
 * @aspect RefResolverHelpers
 * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:700
 */
 interface Unresolved$Node$Interface {

     
    String getUnresolved$Token();

     
    boolean getUnresolved$ResolveOpposite();
}
