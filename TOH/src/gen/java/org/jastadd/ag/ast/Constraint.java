/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\ag.ast:1
 * @astdecl Constraint : ASTNode ::= Connective*;
 * @production Constraint : {@link ASTNode} ::= <span class="component">{@link Connective}*</span>;

 */
public class Constraint extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect Move
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\main\\jastadd\\hanoi\\Move.jadd:2
   */
  public boolean oddTurnMove(Pillar P0, Pillar P1){
      /*for every odd turn(turn==true), move the smallest disk D0 in sequence.                                           |-----------Right:DisksOnPillar(P0)
      Therefore, ValidTarget checks for a move from P0 to P1, when D0 is on the top of P0.         |------If():ComparFunction()------Left:ConstantNumM0(0)
      Conjunction()-------------hasSmallestDisk()---------R0()-----------------F0()----------------|------Then():P0TopDiskSize()
                            |                         TopDiskSizeEq1        IfThenElse             |------Else():ConstantNumM1(-1)
                            |                               |
                            |                               |----------ConstantNum1(1)
                            |---ValidTarget()---------R1()---------P1.ID()
                                                     P1EqTarget
                                                       |
                                                       |-----Mod0()------Left:Plus0()------Left:Plus1()------Left:Mod1()------TotalDiskAmount();
                                                              |              |               |                |------------ConstantNum2(2)
                                                              |              |               |--------ConstantNum1(1)
                                                              |              |------P0.ID
                                                              |------ConstantNum3(3)*/
            Conjunction ValidMove=new Conjunction();

            Atom hasSmallestDisk=new Atom();//if the smallest disk is on this pillar
            Equal TopDiskSizeEq1= new Equal();
            IfThenElse TopDiskChecher=new IfThenElse();
            CompareFunction PillarHasDisk=new CompareFunction();
            TopDiskSize P0TopDiskSize=new TopDiskSize();
            ConstantNum NumM1=new ConstantNum();
            ConstantNum Num1=new ConstantNum();
            ConstantNum Num0=new ConstantNum();
            DisksOnPillar DisksOnP0=new DisksOnPillar();
            
            Atom ValidTarget=new Atom();//check if the move sequence from P0 to P1 is valid
            Equal P1EqTarget= new Equal();
            PillarID P1ID=new PillarID();
            Mod Mod0=new Mod();
            Mod Mod1=new Mod();
            Plus Plus0=new Plus();
            Plus Plus1=new Plus();
            TotalDiskAmount DisksOverall=new TotalDiskAmount();
            PillarID P0ID=new PillarID();
            ConstantNum Num2=new ConstantNum();
            ConstantNum Num3=new ConstantNum();
            
            ValidMove.setLeft(hasSmallestDisk);
            ValidMove.setRight(ValidTarget);

            NumM1.setNum(-1);
            Num0.setNum(0);
            Num1.setNum(1);
            Num2.setNum(2);
            Num3.setNum(3);
            
            TopDiskSizeEq1.setLeft(TopDiskChecher);
            TopDiskChecher.setIf(PillarHasDisk);
            PillarHasDisk.setLeft(Num0);
            PillarHasDisk.setRight(DisksOnP0);
            DisksOnP0.setRel(P0);
            TopDiskChecher.setThen(P0TopDiskSize);
            P0TopDiskSize.setRel(P0);
            TopDiskChecher.setElse(NumM1);
            TopDiskSizeEq1.setRight(Num1);
            hasSmallestDisk.setRelation(TopDiskSizeEq1);

            ValidTarget.setRelation(P1EqTarget);
            P1EqTarget.setLeft(P1ID);
            P1ID.setRel(P1);
            P1EqTarget.setRight(Mod0);
            Mod0.setLeft(Plus0);
            Mod0.setRight(Num3);
            Plus0.setLeft(Plus1);
            Plus0.setRight(P0ID);
            P0ID.setRel(P0);
            Plus1.setLeft(Mod1);
            Plus1.setRight(Num1);
            Mod1.setLeft(DisksOverall);
            DisksOverall.setRel(P0);
            Mod1.setRight(Num2);
            
            this.addConnective(ValidMove);
            if(this.getConnective(0).eval()){
              System.out.println("Odd turn: P" + P0.ID() +".hahasSmallestDisk0 && (P" + P0.ID() +".target == P" + P1.ID()+")");
              return true;//odd turn and valid move
            }else{
              return false;
            }
          }
  /**
   * @aspect Move
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\main\\jastadd\\hanoi\\Move.jadd:84
   */
  public boolean evenTurnMove(Pillar P0, Pillar P1){
        /*for every even turn(turn==false), move the disk D on top of P0 to P1:
        D is not D0(the smallest disk).
        There is always one D can be moved in the even turn according to the game rules.
        Statements:
        1.P0 is not empty: not OND().
        2.When condition 1 holds, P1 can be empty: TND().
        3.When 1 holds and 2 doesn\u9225\u6a9b hold, D0 < D1(for the disks on the top of P0 and P1 respectively).
        Therefore, we want not OND() and (TND() or ValidTarget()) to be true.
                                                       |-----------hasSmallestDisk()-----------------R0---------------T0()
                                    C1                 |C3                               |----------------T3()
        Conjunction()-------------Negation()------Disjunction()----OND()----------------R1()----------------T0()
            C0                |                                                            |----------------T2()
                              |---Disjunction()-----------TND()--------------R2()---------T1()
                                      |C2                                       |-----------T2()
                                      |------------------ValidTarget()-----------------R3()---------T0()
                                                                                |---------T1*/
            Conjunction C0=new Conjunction();
            Negation C1=new Negation();
            Disjunction C2=new Disjunction();
            Disjunction C3=new Disjunction();
            Atom hasSmallestDisk=new Atom();//if the smallest disk is on this pillar
            Atom OND=new Atom();//origin has no disk
            Atom TND=new Atom();//target has no disk
            Atom ValidTarget=new Atom();//check the size, if the disk on the top of origin has smaller size than the one on target
        
            C0.setLeft(C1);
            C0.setRight(C2);
            C1.setConnective(C3);
            C2.setLeft(TND);
            C2.setRight(ValidTarget);
            C3.setLeft(hasSmallestDisk);
            C3.setRight(OND);
        
            Term1 T0=new Term1();//return size of top disk, return -1 when no disk.
            Term1 T1=new Term1();
            ConstantNum T2=new ConstantNum();
            ConstantNum T3=new ConstantNum();
        
            T0.setRel(P0);
            T1.setRel(P1);
            T2.setNum(0);
            T3.setNum(1);
        
            Equal R0=new Equal();
            Compare R1=new Compare();
            Compare R2=new Compare();
            Compare R3=new Compare();
        
            R0.setLeft(T0);
            R0.setRight(T3);
            R1.setLeft(T0);
            R1.setRight(T2);
            R2.setLeft(T1);
            R2.setRight(T2);
            R3.setLeft(T0);
            R3.setRight(T1);
            hasSmallestDisk.setRelation(R0);
            OND.setRelation(R1);
            TND.setRelation(R2);
            ValidTarget.setRelation(R3);
            
            this.addConnective(C0);
            if(this.getConnective(0).eval()){
              System.out.println("Even turn: !(P" + P0.ID() +".#disk == 0 || P" + P0.ID() +".hahasSmallestDisk0) && (P" + P1.ID() +".#disk == 0 || P" + P0.ID() +".topDisk < P" + P1.ID() +".topDisk)");
              return true;//even turn and valid move
            }
            return false;
          }
  /**
   * @aspect MoveAlter
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\main\\jastadd\\hanoi\\MoveAlter.jadd:2
   */
  public boolean oddTurnMoveAlter(Pillar P0, Pillar P1){
      /*for every odd turn(turn==true), move the smallest disk D0 in sequence.                                           |-----------Right:DisksOnPillar(P0)
      Therefore, ValidTarget checks for a move from P0 to P1, when D0 is on the top of P0.         |------If():ComparFunction()------Left:ConstantNumM0(0)
      Conjunction()-------------hasSmallestDisk()---------R0()-----------------F0()----------------|------Then():P0TopDiskSize()
                            |                         TopDiskSizeEq1        IfThenElse             |------Else():ConstantNumM1(-1)
                            |                               |
                            |                               |----------ConstantNum1(1)
                            |---ValidTarget()---------R1()---------P1.ID()
                                                     P1EqTarget
                                                       |
                                                       |-----Mod0()------Left:Plus0()------Left:Plus1()------Left:Mod1()------TotalDiskAmount();
                                                              |              |               |                |------------ConstantNum2(2)
                                                              |              |               |--------ConstantNum1(1)
                                                              |              |------P0.ID
                                                              |------ConstantNum3(3)*/
            

            
            
            TopDiskSize P0TopDiskSize=new TopDiskSize(P0); 
            ConstantNum NumM1=new ConstantNum(-1);
            ConstantNum Num1=new ConstantNum(1);
            ConstantNum Num0=new ConstantNum(0);
            ConstantNum Num2=new ConstantNum(2);
            ConstantNum Num3=new ConstantNum(3);
            DisksOnPillar DisksOnP0=new DisksOnPillar(P0);
            PillarID P0ID=new PillarID(P0);
            PillarID P1ID=new PillarID(P1);
            TotalDiskAmount DisksOverall=new TotalDiskAmount(P0);//return the number of disks set at the beginning
            CompareFunction PillarHasDisk=new CompareFunction(Num0, DisksOnP0);
            IfThenElse TopDiskChecher=new IfThenElse(PillarHasDisk, P0TopDiskSize, NumM1);//if the origin pillar
                                                                            // is not emapty
                                                                           //return the size of its top disk
                                                                           //else return -1 means no such disk
            Equal TopDiskSizeEq1= new Equal(TopDiskChecher, Num1);//the size of the disk on the top equals 1,
                                                                  //indicates this is the smallest disk
            Atom hasSmallestDisk=new Atom(TopDiskSizeEq1);//if the smallest disk is on this pillar
            Mod Mod1=new Mod(DisksOverall, Num2);//check if we have odd or even disks in the game
            Plus Plus1=new Plus(Mod1, Num1);//a intermediat parameter for calculating the target pillar in an odd turn
            Plus Plus0=new Plus(Plus1, P0ID);//a parameter for calculating the target pillar in an odd turn
            Mod Mod0=new Mod(Plus0, Num3);//calculating the target pillar in an odd turn
            
            Equal P1EqTarget= new Equal(P1ID, Mod0);
            Atom ValidTarget=new Atom(P1EqTarget);//check if the move sequence from P0 to P1 is valid
            
            
           
            Conjunction ValidMove=new Conjunction(hasSmallestDisk, ValidTarget);
            
            
            


            this.addConnective(ValidMove);
            if(this.getConnective(0).eval()){
              System.out.println("Odd turn: P" + P0.ID() +".hahasSmallestDisk0 && (P" + P0.ID() +".target == P" + P1.ID()+")");
              return true;//odd turn and valid move
            }else{
              return false;
            }
          }
  /**
   * @aspect MoveAlter
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\main\\jastadd\\hanoi\\MoveAlter.jadd:63
   */
  public boolean evenTurnMoveAlter(Pillar P0, Pillar P1){
            /*for every even turn(turn==false), move the disk D on top of P0 to P1:
            D is not D0(the smallest disk).
            There is always one D can be moved in the even turn according to the game rules.
            Statements:
            1.P0 is not empty: not OND().
            2.When condition 1 holds, P1 can be empty: TND().
            3.When 1 holds and 2 doesn\u9225\u6a9b hold, D0 < D1(for the disks on the top of P0 and P1 respectively).
            Therefore, we want not OND() and (TND() or ValidTarget()) to be true.
                                                           |-----------hasSmallestDisk()-----------------R0---------------T0()
                                        C1                 |C3                               |----------------T3()
            Conjunction()-------------Negation()------Disjunction()----OND()----------------R1()----------------T0()
                C0                |                                                            |----------------T2()
                                  |---Disjunction()-----------TND()--------------R2()---------T1()
                                          |C2                                       |-----------T2()
                                          |------------------ValidTarget()-----------------R3()---------T0()
                                                                                    |---------T1*/
                Term1 T0=new Term1(P0);//return size of top disk, return -1 when no disk.
                Term1 T1=new Term1(P1);
                ConstantNum T2=new ConstantNum(0);
                ConstantNum T3=new ConstantNum(1);

                Equal R0=new Equal(T0, T3);
                Compare R1=new Compare(T0, T2);
                Compare R2=new Compare(T1, T2);
                Compare R3=new Compare(T0, T1);

                Atom hasSmallestDisk=new Atom(R0);//if the smallest disk is on this pillar
                Atom OND=new Atom(R1);//origin has no disk
                Atom TND=new Atom(R2);//target has no disk
                Atom ValidTarget=new Atom(R3);//check the size, if the disk on the top of origin has smaller size than the one on target
            
                
                Disjunction C3=new Disjunction(hasSmallestDisk, OND);                                                               
                Disjunction C2=new Disjunction(TND, ValidTarget);
                Negation C1=new Negation(C3);
                Conjunction C0=new Conjunction(C1, C2);
    
                this.addConnective(C0);
                if(this.getConnective(0).eval()){
                  System.out.println("Even turn: !(P" + P0.ID() +".#disk == 0 || P" + P0.ID() +".hahasSmallestDisk0) && (P" + P1.ID() +".#disk == 0 || P" + P0.ID() +".topDisk < P" + P1.ID() +".topDisk)");
                  return true;//even turn and valid move
                }
                return false;
              }
  /**
   * @aspect MoveTo
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\main\\jastadd\\hanoi\\MoveTo.jadd:2
   */
  public int check(Pillar P0, Pillar P1, boolean turn){
    if(turn==true){
      if(this.oddTurn(P0, P1)){
        return 1;
      }
    }
    if(this.evenTurn(P0, P1)){
      return 2;
    }
    return 0;
  }
  /**
   * @aspect MoveTo
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\main\\jastadd\\hanoi\\MoveTo.jadd:30
   */
  public boolean oddTurn(Pillar P0, Pillar P1){
    /*for every odd turn(turn==true), move the smallest disk D0 in sequence.
    Therefore, CS checks for a move from P0 to P1, when D0 is on the top of P0.
    Conjunction()-------------SD()---------R0()---------T0()
                          |                  |----------T1()
                          |---CS()---------R1()---------T2()
                                                  |
                                                  |-----T3()*/
          Conjunction C=new Conjunction();
          Atom SD=new Atom();//if the smallest disk is on this pillar
          Atom CS=new Atom();//check if the move sequence from P0 to P1 is valid
          Equal R0= new Equal();
          Equal R1= new Equal();
          Term1 T0=new Term1();//return size of top disk, return -1 when no disk
          ConstantNum T1=new ConstantNum();
          Term2 T2=new Term2();//return the target pillar's ID for this pillar in odd turns
          Term3 T3=new Term3();//return the ID for this pillar
          T0.setRel(P0);
          T1.setNum(1);
          T2.setRel(P0);
          T3.setRel(P1);
          R0.setLeft(T0);
          R0.setRight(T1);
          R1.setLeft(T2);
          R1.setRight(T3);
    
          SD.setRelation(R0);
          CS.setRelation(R1);
          C.setLeft(SD);
          C.setRight(CS);
          this.addConnective(C);
          if(this.getConnective(0).eval()){
            System.out.println("Odd turn: P" + P0.ID() +".hasD0 && (P" + P0.ID() +".target == P" + P1.ID()+")");
            return true;//odd turn and valid move
          }else{
            return false;
          }
        }
  /**
   * @aspect MoveTo
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\main\\jastadd\\hanoi\\MoveTo.jadd:68
   */
  public boolean evenTurn(Pillar P0, Pillar P1){
      /*for every even turn(turn==false), move the disk D on top of P0 to P1:
      D is not D0(the smallest disk).
      There is always one D can be moved in the even turn according to the game rules.
      Statements:
      1.P0 is not empty: not OND().
      2.When condition 1 holds, P1 can be empty: TND().
      3.When 1 holds and 2 doesn\u9225\u6a9b hold, D0 < D1(for the disks on the top of P0 and P1 respectively).
      Therefore, we want not OND() and (TND() or CS()) to be true.
                                                     |-----------SD()-----------------R0---------------T0()
                                  C1                 |C3                               |----------------T3()
      Conjunction()-------------Negation()------Disjunction()----OND()----------------R1()----------------T0()
          C0                |                                                            |----------------T2()
                            |---Disjunction()-----------TND()--------------R2()---------T1()
                                    |C2                                       |-----------T2()
                                    |------------------CS()-----------------R3()---------T0()
                                                                              |---------T1*/
          Conjunction C0=new Conjunction();
          Negation C1=new Negation();
          Disjunction C2=new Disjunction();
          Disjunction C3=new Disjunction();
          Atom SD=new Atom();//if the smallest disk is on this pillar
          Atom OND=new Atom();//origin has no disk
          Atom TND=new Atom();//target has no disk
          Atom CS=new Atom();//check the size, if the disk on the top of origin has smaller size than the one on target
      
          C0.setLeft(C1);
          C0.setRight(C2);
          C1.setConnective(C3);
          C2.setLeft(TND);
          C2.setRight(CS);
          C3.setLeft(SD);
          C3.setRight(OND);
      
          Term1 T0=new Term1();//return size of top disk, return -1 when no disk.
          Term1 T1=new Term1();
          ConstantNum T2=new ConstantNum();
          ConstantNum T3=new ConstantNum();
      
          T0.setRel(P0);
          T1.setRel(P1);
          T2.setNum(0);
          T3.setNum(1);
      
          Equal R0=new Equal();
          Compare R1=new Compare();
          Compare R2=new Compare();
          Compare R3=new Compare();
      
          R0.setLeft(T0);
          R0.setRight(T3);
          R1.setLeft(T0);
          R1.setRight(T2);
          R2.setLeft(T1);
          R2.setRight(T2);
          R3.setLeft(T0);
          R3.setRight(T1);
          SD.setRelation(R0);
          OND.setRelation(R1);
          TND.setRelation(R2);
          CS.setRelation(R3);
          
          this.addConnective(C0);
          if(this.getConnective(0).eval()){
           // System.out.println("Even turn: !(P" + P0.ID() +".#disk == 0 || P" + P0.ID() +".hasD0) && (P" + P1.ID() +".#disk == 0 || P" + P0.ID() +".topDisk < P" + P1.ID() +".topDisk)");
            return true;//even turn and valid move
          }
          return false;
        }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:3
   */
  public static Constraint createRef(String ref) {
    Unresolved$Constraint unresolvedNode = new Unresolved$Constraint();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:9
   */
  public static Constraint createRefDirection(String ref) {
    Unresolved$Constraint unresolvedNode = new Unresolved$Constraint();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:528
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:721
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\agRefResolver.jadd:727
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Constraint() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[1];  getChild_handler = new ASTNode$DepGraphNode[children.length];
    state().enterConstruction();
    setChild(new JastAddList(), 0);
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:16
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Connective"},
    type = {"JastAddList<Connective>"},
    kind = {"List"}
  )
  public Constraint(JastAddList<Connective> p0) {
state().enterConstruction();
    setChild(p0, 0);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:27
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:35
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  public Constraint clone() throws CloneNotSupportedException {
    Constraint node = (Constraint) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:50
   */
  public Constraint copy() {
    try {
      Constraint node = (Constraint) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:74
   */
  @Deprecated
  public Constraint fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:84
   */
  public Constraint treeCopyNoTransform() {
    Constraint tree = (Constraint) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:105
   */
  public Constraint treeCopy() {
    Constraint tree = (Constraint) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:120
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:123
   */
  protected void inc_copyHandlers(Constraint copy) {
    super.inc_copyHandlers(copy);

  }
  /** @apilevel internal 
   * @declaredat ASTNode:129
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:136
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:138
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:147
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:148
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:156
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:157
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   * Replaces the Connective list.
   * @param list The new list node to be used as the Connective list.
   * @apilevel high-level
   */
  public Constraint setConnectiveList(JastAddList<Connective> list) {
    setChild(list, 0);
    return this;
  }
  /**
   * Retrieves the number of children in the Connective list.
   * @return Number of children in the Connective list.
   * @apilevel high-level
   */
  public int getNumConnective() {
    return getConnectiveList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Connective list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Connective list.
   * @apilevel low-level
   */
  public int getNumConnectiveNoTransform() {
    return getConnectiveListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Connective list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Connective list.
   * @apilevel high-level
   */
  public Connective getConnective(int i) {
    return (Connective) getConnectiveList().getChild(i);
  }
  /**
   * Check whether the Connective list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasConnective() {
    return getConnectiveList().getNumChild() != 0;
  }
  /**
   * Append an element to the Connective list.
   * @param node The element to append to the Connective list.
   * @apilevel high-level
   */
  public Constraint addConnective(Connective node) {
    JastAddList<Connective> list = (parent == null) ? getConnectiveListNoTransform() : getConnectiveList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public Constraint addConnectiveNoTransform(Connective node) {
    JastAddList<Connective> list = getConnectiveListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the Connective list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public Constraint setConnective(Connective node, int i) {
    JastAddList<Connective> list = getConnectiveList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the Connective list.
   * @return The node representing the Connective list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Connective")
  public JastAddList<Connective> getConnectiveList() {
    JastAddList<Connective> list = (JastAddList<Connective>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Connective list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Connective list.
   * @apilevel low-level
   */
  public JastAddList<Connective> getConnectiveListNoTransform() {
    return (JastAddList<Connective>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Connective list without
   * triggering rewrites.
   */
  public Connective getConnectiveNoTransform(int i) {
    return (Connective) getConnectiveListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Connective list.
   * @return The node representing the Connective list.
   * @apilevel high-level
   */
  public JastAddList<Connective> getConnectives() {
    return getConnectiveList();
  }
  /**
   * Retrieves the Connective list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Connective list.
   * @apilevel low-level
   */
  public JastAddList<Connective> getConnectivesNoTransform() {
    return getConnectiveListNoTransform();
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
