package org.jastadd.ag.ast;

import java.util.*;
/**
 * @ast class
 * @aspect RelAstAPI
 * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\ag.jadd:39
 */
public class Pair<T1, T2> extends java.lang.Object {
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\ag.jadd:40
   */
  
    public final T1 _1;
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\ag.jadd:41
   */
  
    public final T2 _2;
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\ag.jadd:42
   */
  
    public Pair(T1 _1, T2 _2) {
      ASTNode.assertNotNull(_1);
      ASTNode.assertNotNull(_2);
      this._1 = _1;
      this._2 = _2;
    }
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\ag.jadd:48
   */
  
    public boolean equals(Object other) {
      if (other instanceof Pair) {
        Pair<?,?> p = (Pair<?,?>) other;
        return _1.equals(p._1) && _2.equals(p._2);
      } else {
        return false;
      }
    }
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\ConstraintModel\\TOH\\src\\gen\\jastadd\\ag.jadd:56
   */
  
    public int hashCode() {
      return 31*_1.hashCode() + _2.hashCode();
    }

}
