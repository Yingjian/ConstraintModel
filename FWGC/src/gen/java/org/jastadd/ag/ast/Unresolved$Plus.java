package org.jastadd.ag.ast;

import java.util.*;
/**
 * @ast class
 * @aspect RefResolverHelpers
 * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:1493
 */
 class Unresolved$Plus extends Plus implements Unresolved$Node$Interface {
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:1494
   */
  
    private String unresolved$Token;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:1495
   */
  
    public String getUnresolved$Token() {
      return unresolved$Token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:1498
   */
  
    void setUnresolved$Token(String token) {
      this.unresolved$Token = token;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:1501
   */
  
    private boolean unresolved$ResolveOpposite;
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:1502
   */
  
    public boolean getUnresolved$ResolveOpposite() {
      return unresolved$ResolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:1505
   */
  
    void setUnresolved$ResolveOpposite(boolean resolveOpposite) {
      this.unresolved$ResolveOpposite = resolveOpposite;
    }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:1512
   */
  Unresolved$Node$Interface as$Unresolved() {
    return this;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:1518
   */
  boolean is$Unresolved() {
    return true;
  }

}
