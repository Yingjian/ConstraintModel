/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\ag.ast:1
 * @astdecl Constraint_Model : ASTNode ::= Formula*;
 * @production Constraint_Model : {@link ASTNode} ::= <span class="component">{@link Formula}*</span>;

 */
public class Constraint_Model extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect in_game_constraints
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\main\\jastadd\\UseCase\\In_Game_Constraints.jadd:2
   */
  public boolean Valid_Move_1(Location l){
        Location_has_Farmer R0=new Location_has_Farmer(l);
        Location_has_Wolf R1=new Location_has_Wolf(l);
        Location_has_Goat R2=new Location_has_Goat(l);
        Location_has_Cabbage R3=new Location_has_Cabbage(l);
        Atom P1=new Atom(R0);
        Atom has_Wolf=new Atom(R1);
        Atom has_Goat=new Atom(R2);
        Atom has_Cabbage=new Atom(R3);
        Conjunction W_G = new Conjunction(has_Wolf, has_Goat);
        Conjunction G_C = new Conjunction(has_Goat, has_Cabbage);
        Disjunction Conflict =new Disjunction(W_G, G_C);
        Negation Conflict_Free = new Negation(Conflict);
        Conjunction Validity=new Conjunction(P1, Conflict_Free);
        this.addFormula(Validity);
        if(Validity.evaluation()==false){
                if(W_G.evaluation()){
                        System.out.println("The farmer should take the wolf or the goat, or the wolf will eat the goat.");
                }
                if(G_C.evaluation()){
                        System.out.println("The farmer should take the goat or the cabbage, or the goat will eat the cabbage.");
                }
        }
        return this.getFormula(this.getNumFormula()-1).evaluation();
      }
  /**
   * @aspect in_game_constraints
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\main\\jastadd\\UseCase\\In_Game_Constraints.jadd:27
   */
  public boolean Valid_Move_2(Location Departure_Location, int Passenger){
        //the Unary_Relations to check if the Farmer, the Wolf, the Goat, or the Cabbage is at the Departure_Location.
        Location_has_Farmer has_Farmer_Relation =  new Location_has_Farmer(Departure_Location);
        Location_has_Wolf has_Wolf_Relation =      new Location_has_Wolf(Departure_Location);
        Location_has_Goat has_Goat_Relation =      new Location_has_Goat(Departure_Location);
        Location_has_Cabbage has_Cabbage_Relation =new Location_has_Cabbage(Departure_Location);
//declare relevant Atoms for the above Unary_Relations.
        Atom has_Farmer          = new Atom(has_Farmer_Relation);//P1
        Atom has_Wolf            = new Atom(has_Wolf_Relation);
        Atom has_Goat            = new Atom(has_Goat_Relation);
        Atom has_Cabbage         = new Atom(has_Cabbage_Relation);
//construct P2, P3 and P4
        Conjunction Cabbage_Goat = new Conjunction(has_Cabbage, has_Goat);//P2
        Conjunction Wolf_Goat    = new Conjunction(has_Wolf, has_Goat);//P3
        //declare the constant number representing the species, 0 for Wolf, 1 for Goat, and 2 for Cabbage.
        Constant_Num is_Wolf           = new Constant_Num(0);                                    
        Constant_Num is_Goat           = new Constant_Num(1);
        Constant_Num is_Cabbage        = new Constant_Num(2);
//get the constant number representing the species for Passenger.
        Constant_Num Passenger_Species = new Constant_Num(Passenger);
//the Binary_Relations to check if the proposed Passenger is the Wolf, the Goat or the Cabbage.
        Equal Passenger_is_Wolf        = new Equal(Passenger_Species, is_Wolf);
        Equal Passenger_is_Goat        = new Equal(Passenger_Species, is_Goat);
        Equal Passenger_is_Cabbage     = new Equal(Passenger_Species, is_Cabbage);
//declare the relevant Atoms for the above Binary_Relations.
        Atom Proposed_Wolf             = new Atom(Passenger_is_Wolf);//the first clause of P_7
        Atom Proposed_Goat             = new Atom(Passenger_is_Goat);//P_6
        Atom Proposed_Cabbage          = new Atom(Passenger_is_Cabbage);//the first clause of P_8
//check if the proposed Passenger is valid.
        Conjunction Proposed_Wolf_and_Ready =   new Conjunction(has_Wolf, Proposed_Wolf);
        Conjunction Proposed_Goat_and_Ready =   new Conjunction(has_Goat, Proposed_Goat);
        Conjunction Proposed_Cabbage_and_Ready =new Conjunction(has_Cabbage, Proposed_Cabbage);
//the first disjunct and the complete disjunction to check the validity of the Passenger.
        Disjunction Passenger_Valid_WG = new Disjunction(Proposed_Wolf_and_Ready, Proposed_Goat_and_Ready);
        Disjunction Passenger_Valid = new Disjunction(Passenger_Valid_WG, Proposed_Cabbage_and_Ready);//P5
//the second clause of P8, violate the case that the Wolf and the Goat are at the Departure_Location at the same time.
        Negation not_Wolf_Goat    = new Negation(Wolf_Goat);
//the second clause of P7, violate the case that the Cabbage and the Goat are at the Departure_Location at the same time.
        Negation not_Cabbage_Goat = new Negation(Cabbage_Goat);
//the complete P_4 and P_5.
        Conjunction Proposed_Wolf_Valid =    new Conjunction(Proposed_Wolf, not_Cabbage_Goat);//P_4
        Conjunction Proposed_Cabbage_Valid = new Conjunction(Proposed_Cabbage, not_Wolf_Goat);//P_5
//construct  P_1 && P_2 && (P_3 || P_4 || P_5)
        Disjunction P3_P4    = new Disjunction(Proposed_Goat, Proposed_Wolf_Valid);
        Disjunction P3_P4_P5 = new Disjunction(P3_P4, Proposed_Cabbage_Valid);
        Conjunction P1_2     = new Conjunction(has_Farmer, Passenger_Valid);
        Conjunction Valid_Ferry_With_Passenger = new Conjunction(P1_2, P3_P4_P5);
   /*     if(Valid_Ferry_With_Passenger.evaluation()==false){
                if(Passenger_Valid.evaluation()==false){
                        if(Proposed_Wolf){
                                System.out.println("The wolf is not here.");
                        }
                        if(Proposed_Goat){
                                System.out.println("The goat is not here.");
                        }
                        if(Proposed_Cabbage){
                                System.out.println("The cabbage is not here.");
                        } 
                }else{
                        if(Proposed_Wolf_Valid==false){
                                System.out.println("The wolf is not here.");
                        }
                        if(Proposed_Goat){
                                System.out.println("The goat is not here.");
                        }
                        if(Proposed_Cabbage){
                                System.out.println("The cabbage is not here.");
                        } 
                }
                if(G_C.evaluation()){
                        System.out.println("The farmer should take the goat or the cabbage, or the goat will eat the cabbage.");
                }
        }*/
        this.addFormula(Valid_Ferry_With_Passenger);
        return this.getFormula(this.getNumFormula()-1).evaluation();
      }
  /**
   * @aspect in_game_constraints
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\main\\jastadd\\UseCase\\In_Game_Constraints.jadd:103
   */
  public boolean End_Game_Checker(FWGC G){
        Location_has_Farmer has_Farmer_Relation   =  new Location_has_Farmer(G.getNorth());
        Location_has_Wolf has_Wolf_Relation       =  new Location_has_Wolf(G.getNorth());
        Location_has_Goat has_Goat_Relation       =  new Location_has_Goat(G.getNorth());
        Location_has_Cabbage has_Cabbage_Relation = new Location_has_Cabbage(G.getNorth());

        Atom has_Farmer         = new Atom(has_Farmer_Relation);//P1
        Atom has_Wolf           = new Atom(has_Wolf_Relation);
        Atom has_Goat           = new Atom(has_Goat_Relation);
        Atom has_Cabbage        = new Atom(has_Cabbage_Relation);

        Conjunction FW          = new Conjunction(has_Farmer, has_Wolf);
        Conjunction FWG         = new Conjunction(FW, has_Goat);
        Conjunction End_of_Game = new Conjunction(FWG, has_Cabbage);
        this.addFormula(End_of_Game);
        return this.getFormula(this.getNumFormula()-1).evaluation();
      }
  /**
   * @aspect in_game_constraints
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\main\\jastadd\\UseCase\\In_Game_Constraints.jadd:127
   */
  public boolean Valid_Move_2alter(Location Departure_Location, int Passenger){
        //the Unary_Relations to check if the Farmer, the Wolf, the Goat, or the Cabbage is at the Departure_Location.
        Location_has_Farmer has_Farmer_Relation =  new Location_has_Farmer(Departure_Location);
        Location_has_Wolf has_Wolf_Relation =      new Location_has_Wolf(Departure_Location);
        Location_has_Goat has_Goat_Relation =      new Location_has_Goat(Departure_Location);
        Location_has_Cabbage has_Cabbage_Relation =new Location_has_Cabbage(Departure_Location);
//declare relevant Atoms for the above Unary_Relations.
        Atom has_Farmer          = new Atom(has_Farmer_Relation);//P1
        Atom has_Wolf            = new Atom(has_Wolf_Relation);
        Atom has_Goat            = new Atom(has_Goat_Relation);
        Atom has_Cabbage         = new Atom(has_Cabbage_Relation);
//construct P2, P3 and P4
        Conjunction Cabbage_Goat = new Conjunction(has_Cabbage, has_Goat);//P2
        Conjunction Wolf_Goat    = new Conjunction(has_Wolf, has_Goat);//P3
        //declare the constant number representing the species, 0 for Wolf, 1 for Goat, and 2 for Cabbage.
        Constant_Num is_Wolf           = new Constant_Num(0);                                    
        Constant_Num is_Goat           = new Constant_Num(1);
        Constant_Num is_Cabbage        = new Constant_Num(2);
//get the constant number representing the species for Passenger.
        Constant_Num Passenger_Species = new Constant_Num(Passenger);
//the Binary_Relations to check if the proposed Passenger is the Wolf, the Goat or the Cabbage.
        Equal Passenger_is_Wolf        = new Equal(Passenger_Species, is_Wolf);
        Equal Passenger_is_Goat        = new Equal(Passenger_Species, is_Goat);
        Equal Passenger_is_Cabbage     = new Equal(Passenger_Species, is_Cabbage);
//declare the relevant Atoms for the above Binary_Relations.
        Atom Proposed_Wolf             = new Atom(Passenger_is_Wolf);//the first clause of P_7
        Atom Proposed_Goat             = new Atom(Passenger_is_Goat);//P_6
        Atom Proposed_Cabbage          = new Atom(Passenger_is_Cabbage);//the first clause of P_8
//check if the proposed Passenger is valid.
        Conjunction Proposed_Wolf_and_Ready =   new Conjunction(has_Wolf, Proposed_Wolf);
        Conjunction Proposed_Goat_and_Ready =   new Conjunction(has_Goat, Proposed_Goat);
        Conjunction Proposed_Cabbage_and_Ready =new Conjunction(has_Cabbage, Proposed_Cabbage);
//the first disjunct and the complete disjunction to check the validity of the Passenger.
        Disjunction Passenger_Valid_WG = new Disjunction(Proposed_Wolf_and_Ready, Proposed_Goat_and_Ready);
        Disjunction Passenger_Valid = new Disjunction(Passenger_Valid_WG, Proposed_Cabbage_and_Ready);//P5
//the second clause of P8, violate the case that the Wolf and the Goat are at the Departure_Location at the same time.
        Negation not_Wolf_Goat    = new Negation(Wolf_Goat);
//the second clause of P7, violate the case that the Cabbage and the Goat are at the Departure_Location at the same time.
        Negation not_Cabbage_Goat = new Negation(Cabbage_Goat);
//the complete P_4 and P_5.
        Implication Proposed_Wolf_Valid =    new Implication(Proposed_Wolf, not_Cabbage_Goat);//P_4
        Implication Proposed_Cabbage_Valid = new Implication(Proposed_Cabbage, not_Wolf_Goat);//P_5
//construct  P_1 && P_2 && (P_3 || P_4 || P_5)
        Conjunction P3_P4    = new Conjunction(Proposed_Cabbage_Valid, Proposed_Wolf_Valid);
        Disjunction P3_P4_P5 = new Disjunction(P3_P4, Proposed_Goat);
        Conjunction P1_2     = new Conjunction(has_Farmer, Passenger_Valid);
        Conjunction Valid_Ferry_With_Passenger = new Conjunction(P1_2, P3_P4_P5);
        if(Valid_Ferry_With_Passenger.evaluation()==false){
                if(Passenger_Valid.evaluation()==false){
                        if(Proposed_Wolf.evaluation()){
                                System.out.println("The wolf is not here.");
                        }
                        if(Proposed_Goat.evaluation()){
                                System.out.println("The goat is not here.");
                        }
                        if(Proposed_Cabbage.evaluation()){
                                System.out.println("The cabbage is not here.");
                        } 
                }else{
                        if(Proposed_Wolf_Valid.evaluation()==false){
                                System.out.println("The goat will eat the cabbage.");
                        }
                        if(Proposed_Cabbage_Valid.evaluation()==false){
                                System.out.println("The wolf will eat the goat.");
                        } 
                }
        }
        this.addFormula(Valid_Ferry_With_Passenger);
        return this.getFormula(this.getNumFormula()-1).evaluation();
      }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:3
   */
  public static Constraint_Model createRef(String ref) {
    Unresolved$Constraint_Model unresolvedNode = new Unresolved$Constraint_Model();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:9
   */
  public static Constraint_Model createRefDirection(String ref) {
    Unresolved$Constraint_Model unresolvedNode = new Unresolved$Constraint_Model();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:636
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:865
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:871
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Constraint_Model() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[1];  getChild_handler = new ASTNode$DepGraphNode[children.length];
    state().enterConstruction();
    setChild(new JastAddList(), 0);
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:16
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Formula"},
    type = {"JastAddList<Formula>"},
    kind = {"List"}
  )
  public Constraint_Model(JastAddList<Formula> p0) {
state().enterConstruction();
    setChild(p0, 0);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:27
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 1;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:35
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  public Constraint_Model clone() throws CloneNotSupportedException {
    Constraint_Model node = (Constraint_Model) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:50
   */
  public Constraint_Model copy() {
    try {
      Constraint_Model node = (Constraint_Model) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:74
   */
  @Deprecated
  public Constraint_Model fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:84
   */
  public Constraint_Model treeCopyNoTransform() {
    Constraint_Model tree = (Constraint_Model) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:105
   */
  public Constraint_Model treeCopy() {
    Constraint_Model tree = (Constraint_Model) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:120
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:123
   */
  protected void inc_copyHandlers(Constraint_Model copy) {
    super.inc_copyHandlers(copy);

  }
  /** @apilevel internal 
   * @declaredat ASTNode:129
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:136
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:138
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:147
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:148
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:156
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:157
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   * Replaces the Formula list.
   * @param list The new list node to be used as the Formula list.
   * @apilevel high-level
   */
  public Constraint_Model setFormulaList(JastAddList<Formula> list) {
    setChild(list, 0);
    return this;
  }
  /**
   * Retrieves the number of children in the Formula list.
   * @return Number of children in the Formula list.
   * @apilevel high-level
   */
  public int getNumFormula() {
    return getFormulaList().getNumChild();
  }
  /**
   * Retrieves the number of children in the Formula list.
   * Calling this method will not trigger rewrites.
   * @return Number of children in the Formula list.
   * @apilevel low-level
   */
  public int getNumFormulaNoTransform() {
    return getFormulaListNoTransform().getNumChildNoTransform();
  }
  /**
   * Retrieves the element at index {@code i} in the Formula list.
   * @param i Index of the element to return.
   * @return The element at position {@code i} in the Formula list.
   * @apilevel high-level
   */
  public Formula getFormula(int i) {
    return (Formula) getFormulaList().getChild(i);
  }
  /**
   * Check whether the Formula list has any children.
   * @return {@code true} if it has at least one child, {@code false} otherwise.
   * @apilevel high-level
   */
  public boolean hasFormula() {
    return getFormulaList().getNumChild() != 0;
  }
  /**
   * Append an element to the Formula list.
   * @param node The element to append to the Formula list.
   * @apilevel high-level
   */
  public Constraint_Model addFormula(Formula node) {
    JastAddList<Formula> list = (parent == null) ? getFormulaListNoTransform() : getFormulaList();
    list.addChild(node);
    return this;
  }
  /** @apilevel low-level 
   */
  public Constraint_Model addFormulaNoTransform(Formula node) {
    JastAddList<Formula> list = getFormulaListNoTransform();
    list.addChild(node);
    return this;
  }
  /**
   * Replaces the Formula list element at index {@code i} with the new node {@code node}.
   * @param node The new node to replace the old list element.
   * @param i The list index of the node to be replaced.
   * @apilevel high-level
   */
  public Constraint_Model setFormula(Formula node, int i) {
    JastAddList<Formula> list = getFormulaList();
    list.setChild(node, i);
    return this;
  }
  /**
   * Retrieves the Formula list.
   * @return The node representing the Formula list.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.ListChild(name="Formula")
  public JastAddList<Formula> getFormulaList() {
    JastAddList<Formula> list = (JastAddList<Formula>) getChild(0);
    return list;
  }
  /**
   * Retrieves the Formula list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Formula list.
   * @apilevel low-level
   */
  public JastAddList<Formula> getFormulaListNoTransform() {
    return (JastAddList<Formula>) getChildNoTransform(0);
  }
  /**
   * @return the element at index {@code i} in the Formula list without
   * triggering rewrites.
   */
  public Formula getFormulaNoTransform(int i) {
    return (Formula) getFormulaListNoTransform().getChildNoTransform(i);
  }
  /**
   * Retrieves the Formula list.
   * @return The node representing the Formula list.
   * @apilevel high-level
   */
  public JastAddList<Formula> getFormulas() {
    return getFormulaList();
  }
  /**
   * Retrieves the Formula list.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The node representing the Formula list.
   * @apilevel low-level
   */
  public JastAddList<Formula> getFormulasNoTransform() {
    return getFormulaListNoTransform();
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
