/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\ag.ast:43
 * @astdecl FWGC : ASTNode ::= South North;
 * @production FWGC : {@link ASTNode} ::= <span class="component">{@link South}</span> <span class="component">{@link North}</span>;

 */
public class FWGC extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect init
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\main\\jastadd\\UseCase\\init.jadd:2
   */
  public void init(){
        South S = new South();
        North N = new North();
        Farmer F = new Farmer();
        Wolf W = new Wolf();
        Goat G = new Goat();
        Cabbage C = new Cabbage();
        W.setSpecies(0);
        G.setSpecies(1);
        C.setSpecies(2);
        S.setFarmer(F);
        S.setWolf(W);
        S.setGoat(G);
        S.setCabbage(C);
        this.setSouth(S);
        this.setNorth(N);
    }
  /**
   * @aspect init
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\main\\jastadd\\UseCase\\init.jadd:19
   */
  public boolean end(){
        Constraint_Model cm =new Constraint_Model();
        return cm.End_Game_Checker(this);
    }
  /**
   * @aspect init
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\main\\jastadd\\UseCase\\init.jadd:23
   */
  public void moveFarmer(Location l1, Location l2){
        l2.setFarmer(l1.getFarmer());
        l1.getFarmerOpt().removeChild(0);
	}
  /**
   * @aspect init
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\main\\jastadd\\UseCase\\init.jadd:27
   */
  public void moveWolf(Location l1, Location l2){
        l2.setWolf(l1.getWolf());
        l1.getWolfOpt().removeChild(0);
	}
  /**
   * @aspect init
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\main\\jastadd\\UseCase\\init.jadd:31
   */
  public void moveGoat(Location l1, Location l2){
        l2.setGoat(l1.getGoat());
        l1.getGoatOpt().removeChild(0);
	}
  /**
   * @aspect init
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\main\\jastadd\\UseCase\\init.jadd:35
   */
  public void moveCabbage(Location l1, Location l2){
        l2.setCabbage(l1.getCabbage());
        l1.getCabbageOpt().removeChild(0);
	}
  /**
   * @aspect init
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\main\\jastadd\\UseCase\\init.jadd:39
   */
  public void Move(Location l1, Location l2, int Case){
        this.moveFarmer(l1, l2);
        if(Case==1){
            this.moveWolf(l1, l2);
        }else if(Case==2){
            this.moveGoat(l1, l2);
        }else if(Case==3){
            this.moveCabbage(l1, l2);
        }
	}
  /**
   * @aspect init
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\main\\jastadd\\UseCase\\init.jadd:49
   */
  public boolean move(Location l1, Location l2, int Case){
        Constraint_Model cm=new Constraint_Model();
        if(Case==0){
            if(cm.Valid_Move_1(l1)){
                this.Move(l1, l2, Case);
                return true;
            }else{
                return false;
            }	
        }else{
            if(Case==1){
                if(cm.Valid_Move_2alter(l1, 0)){
                    this.Move(l1, l2, Case);
                    return true;
                }else{
                    return false;
                }	
            }
            if(Case==2){
                if(cm.Valid_Move_2alter(l1, 1)){
                    this.Move(l1, l2, Case);
                    return true;
                }else{
                    return false;
                }	
            }
            if(Case==3){
                if(cm.Valid_Move_2alter(l1, 2)){
                    this.Move(l1, l2, Case);
                    return true;
                }else{
                    return false;
                }	
            }
        }
        return false;
	}
  /**
   * @aspect init
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\main\\jastadd\\UseCase\\init.jadd:86
   */
  public void print(){
        System.out.print("South:");
        if(this.getSouth().hasFarmer()){
            System.out.print("Farmer ");
        }
        if(this.getSouth().hasWolf()){
            System.out.print("Wolf ");
        }
        if(this.getSouth().hasGoat()){
            System.out.print("Goat ");
        }
        if(this.getSouth().hasCabbage()){
            System.out.print("Cabbage");
        }
        System.out.println();

        System.out.print("North:");
        if(this.getNorth().hasFarmer()){
            System.out.print("Farmer ");
        }
        if(this.getNorth().hasWolf()){
            System.out.print("Wolf ");
        }
        if(this.getNorth().hasGoat()){
            System.out.print("Goat ");
        }
        if(this.getNorth().hasCabbage()){
            System.out.print("Cabbage");
        }
        System.out.println();
    }
  /**
   * @aspect in_game_constraints
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\main\\jastadd\\UseCase\\In_Game_Constraints.jadd:120
   */
  int Turn(int T){
          return T+1;
      }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:507
   */
  public static FWGC createRef(String ref) {
    Unresolved$FWGC unresolvedNode = new Unresolved$FWGC();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:513
   */
  public static FWGC createRefDirection(String ref) {
    Unresolved$FWGC unresolvedNode = new Unresolved$FWGC();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:805
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:2041
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:2047
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public FWGC() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[2];  getChild_handler = new ASTNode$DepGraphNode[children.length];
    state().enterConstruction();
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:15
   */
  @ASTNodeAnnotation.Constructor(
    name = {"South", "North"},
    type = {"South", "North"},
    kind = {"Child", "Child"}
  )
  public FWGC(South p0, North p1) {
state().enterConstruction();
    setChild(p0, 0);
    setChild(p1, 1);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:27
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 2;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:35
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:39
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:42
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  public FWGC clone() throws CloneNotSupportedException {
    FWGC node = (FWGC) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:50
   */
  public FWGC copy() {
    try {
      FWGC node = (FWGC) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:74
   */
  @Deprecated
  public FWGC fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:84
   */
  public FWGC treeCopyNoTransform() {
    FWGC tree = (FWGC) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:105
   */
  public FWGC treeCopy() {
    FWGC tree = (FWGC) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:120
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:123
   */
  protected void inc_copyHandlers(FWGC copy) {
    super.inc_copyHandlers(copy);

  }
  /** @apilevel internal 
   * @declaredat ASTNode:129
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:136
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:138
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:147
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:148
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:156
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:157
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   * Replaces the South child.
   * @param node The new node to replace the South child.
   * @apilevel high-level
   */
  public FWGC setSouth(South node) {
    setChild(node, 0);
    return this;
  }
  /**
   * Retrieves the South child.
   * @return The current node used as the South child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="South")
  public South getSouth() {
    return (South) getChild(0);
  }
  /**
   * Retrieves the South child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the South child.
   * @apilevel low-level
   */
  public South getSouthNoTransform() {
    return (South) getChildNoTransform(0);
  }
  /**
   * Replaces the North child.
   * @param node The new node to replace the North child.
   * @apilevel high-level
   */
  public FWGC setNorth(North node) {
    setChild(node, 1);
    return this;
  }
  /**
   * Retrieves the North child.
   * @return The current node used as the North child.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Child(name="North")
  public North getNorth() {
    return (North) getChild(1);
  }
  /**
   * Retrieves the North child.
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The current node used as the North child.
   * @apilevel low-level
   */
  public North getNorthNoTransform() {
    return (North) getChildNoTransform(1);
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
