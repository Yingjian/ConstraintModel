/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\ag.ast:38
 * @astdecl Location_Relation : Unary_Relation ::= <_impl_Rel:Location>;
 * @production Location_Relation : {@link Unary_Relation} ::= <span class="component">&lt;_impl_Rel:{@link Location}&gt;</span>;

 */
public abstract class Location_Relation extends Unary_Relation implements Cloneable {
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\ag.jadd:4
   */
  public Location getRel() {
    if (tokenLocation__impl_Rel != null && tokenLocation__impl_Rel.is$Unresolved()) {
      if (tokenLocation__impl_Rel.as$Unresolved().getUnresolved$ResolveOpposite()) {
        setRel(resolveRelByToken(tokenLocation__impl_Rel.as$Unresolved().getUnresolved$Token()));
      } else {
        set_impl_Rel(resolveRelByToken(tokenLocation__impl_Rel.as$Unresolved().getUnresolved$Token()));
      }
    }
    return get_impl_Rel();
  }
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\ag.jadd:14
   */
  public Location_Relation setRel(Location o) {
    assertNotNull(o);
    set_impl_Rel(o);
    return this;
  }
  /**
   * @aspect RelAstAPI
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\ag.jadd:33
   */
  public void computeLowerBoundsViolations(java.util.List<Pair<ASTNode, String>> list) {
    if (getRel() == null) {
      list.add(new Pair<>(this, "Rel"));
    }
    super.computeLowerBoundsViolations(list);
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:447
   */
  public static Location_Relation createRef(String ref) {
    Unresolved$Location_has_Farmer unresolvedNode = new Unresolved$Location_has_Farmer();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:453
   */
  public static Location_Relation createRefDirection(String ref) {
    Unresolved$Location_has_Farmer unresolvedNode = new Unresolved$Location_has_Farmer();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:784
   */
  public void resolveAll() {
    getRel();
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:1901
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:1907
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Location_Relation() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    state().enterConstruction();
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"_impl_Rel"},
    type = {"Location"},
    kind = {"Token"}
  )
  public Location_Relation(Location p0) {
state().enterConstruction();
    set_impl_Rel(p0);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:25
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:33
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public Location_Relation clone() throws CloneNotSupportedException {
    Location_Relation node = (Location_Relation) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:54
   */
  @Deprecated
  public abstract Location_Relation fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:62
   */
  public abstract Location_Relation treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:70
   */
  public abstract Location_Relation treeCopy();
  /** @apilevel internal 
   * @declaredat ASTNode:72
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:75
   */
  protected void inc_copyHandlers(Location_Relation copy) {
    super.inc_copyHandlers(copy);

        if (get_impl_Rel_handler != null) {
          copy.get_impl_Rel_handler = ASTNode$DepGraphNode.createAstHandler(get_impl_Rel_handler, copy);
        }
  }
  /** @apilevel internal 
   * @declaredat ASTNode:84
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:91
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:93
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  if (get_impl_Rel_handler != null) {
    get_impl_Rel_handler.throwAway();
  }
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:105
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:106
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  if (get_impl_Rel_handler != null) {
    get_impl_Rel_handler.cleanupListeners();
  }
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:117
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:118
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   */
  protected ASTNode$DepGraphNode get_impl_Rel_handler = ASTNode$DepGraphNode.createAstHandler(this, "get_impl_Rel", null);
  /**
   * Replaces the lexeme _impl_Rel.
   * @param value The new value for the lexeme _impl_Rel.
   * @apilevel high-level
   */
  public Location_Relation set_impl_Rel(Location value) {
    tokenLocation__impl_Rel = value;
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      get_impl_Rel_handler.notifyDependencies();
    
    
    
    
    }
    return this;
  }
  /** @apilevel internal 
   */
  protected Location tokenLocation__impl_Rel;
  /**
   * Retrieves the value for the lexeme _impl_Rel.
   * @return The value for the lexeme _impl_Rel.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="_impl_Rel")
  public Location get_impl_Rel() {
    
    state().addHandlerDepTo(get_impl_Rel_handler);
    return tokenLocation__impl_Rel;
  }
  /**
   * @attribute syn
   * @aspect RefResolverStubs
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agResolverStubs.jrag:6
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="RefResolverStubs", declaredAt="E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agResolverStubs.jrag:6")
  public Location resolveRelByToken(String id) {
    {
        // default to context-independent name resolution
        return globallyResolveLocationByToken(id);
      }
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
