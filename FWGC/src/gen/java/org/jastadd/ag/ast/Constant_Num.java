/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\ag.ast:37
 * @astdecl Constant_Num : Self_Function ::= <Num:int>;
 * @production Constant_Num : {@link Self_Function} ::= <span class="component">&lt;Num:{@link int}&gt;</span>;

 */
public class Constant_Num extends Self_Function implements Cloneable {
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:435
   */
  public static Constant_Num createRef(String ref) {
    Unresolved$Constant_Num unresolvedNode = new Unresolved$Constant_Num();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:441
   */
  public static Constant_Num createRefDirection(String ref) {
    Unresolved$Constant_Num unresolvedNode = new Unresolved$Constant_Num();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:780
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:1873
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:1879
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Constant_Num() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    state().enterConstruction();
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Num"},
    type = {"int"},
    kind = {"Token"}
  )
  public Constant_Num(int p0) {
state().enterConstruction();
    setNum(p0);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:25
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:33
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public Constant_Num clone() throws CloneNotSupportedException {
    Constant_Num node = (Constant_Num) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:48
   */
  public Constant_Num copy() {
    try {
      Constant_Num node = (Constant_Num) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:72
   */
  @Deprecated
  public Constant_Num fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:82
   */
  public Constant_Num treeCopyNoTransform() {
    Constant_Num tree = (Constant_Num) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:103
   */
  public Constant_Num treeCopy() {
    Constant_Num tree = (Constant_Num) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:118
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:121
   */
  protected void inc_copyHandlers(Constant_Num copy) {
    super.inc_copyHandlers(copy);

        if (getNum_handler != null) {
          copy.getNum_handler = ASTNode$DepGraphNode.createAstHandler(getNum_handler, copy);
        }
  }
  /** @apilevel internal 
   * @declaredat ASTNode:130
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:137
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:139
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  if (getNum_handler != null) {
    getNum_handler.throwAway();
  }
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:151
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:152
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  if (getNum_handler != null) {
    getNum_handler.cleanupListeners();
  }
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:163
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:164
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   */
  protected ASTNode$DepGraphNode getNum_handler = ASTNode$DepGraphNode.createAstHandler(this, "getNum", null);
  /**
   * Replaces the lexeme Num.
   * @param value The new value for the lexeme Num.
   * @apilevel high-level
   */
  public Constant_Num setNum(int value) {
    tokenint_Num = value;
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      getNum_handler.notifyDependencies();
    
    
    
    
    }
    return this;
  }
  /** @apilevel internal 
   */
  protected int tokenint_Num;
  /**
   * Retrieves the value for the lexeme Num.
   * @return The value for the lexeme Num.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Num")
  public int getNum() {
    
    state().addHandlerDepTo(getNum_handler);
    return tokenint_Num;
  }
  /**
   * @attribute syn
   * @aspect Term
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\main\\jastadd\\UseCase\\Term.jrag:4
   */
  @ASTNodeAnnotation.Attribute(kind=ASTNodeAnnotation.Kind.SYN)
  @ASTNodeAnnotation.Source(aspect="Term", declaredAt="E:\\project\\ConstraintModel\\FWGC\\src\\main\\jastadd\\UseCase\\Term.jrag:4")
  public int evaluation() {
    {
            return this.getNum();
        }
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
