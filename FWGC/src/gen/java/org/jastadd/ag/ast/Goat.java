/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\ag.ast:50
 * @astdecl Goat : Fellow ::= <Species:int>;
 * @production Goat : {@link Fellow};

 */
public class Goat extends Fellow implements Cloneable {
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:591
   */
  public static Goat createRef(String ref) {
    Unresolved$Goat unresolvedNode = new Unresolved$Goat();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:597
   */
  public static Goat createRefDirection(String ref) {
    Unresolved$Goat unresolvedNode = new Unresolved$Goat();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:833
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:2237
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:2243
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Goat() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    state().enterConstruction();
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:14
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Species"},
    type = {"int"},
    kind = {"Token"}
  )
  public Goat(int p0) {
state().enterConstruction();
    setSpecies(p0);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:25
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 0;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:33
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:37
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:40
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:43
   */
  public Goat clone() throws CloneNotSupportedException {
    Goat node = (Goat) super.clone();
    return node;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:48
   */
  public Goat copy() {
    try {
      Goat node = (Goat) clone();
      node.parent = null;
      if (children != null) {
        node.children = (ASTNode[]) children.clone();
      }
      node.inc_state = inc_CLONED;
      for (int i = 0; node.children != null && i < node.children.length; i++) {
        node.children[i] = null;
      }
      inc_copyHandlers(node);
      return node;
    } catch (CloneNotSupportedException e) {
      throw new Error("Error: clone not supported for " + getClass().getName());
    }
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:72
   */
  @Deprecated
  public Goat fullCopy() {
    return treeCopyNoTransform();
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:82
   */
  public Goat treeCopyNoTransform() {
    Goat tree = (Goat) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) children[i];
        if (child != null) {
          child = child.treeCopyNoTransform();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:103
   */
  public Goat treeCopy() {
    Goat tree = (Goat) copy();
    if (children != null) {
      for (int i = 0; i < children.length; ++i) {
        ASTNode child = (ASTNode) getChild(i);
        if (child != null) {
          child = child.treeCopy();
          tree.children[i] = child;
          child.parent = tree;
        }
      }
    }
    return tree;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:118
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:121
   */
  protected void inc_copyHandlers(Goat copy) {
    super.inc_copyHandlers(copy);

  }
  /** @apilevel internal 
   * @declaredat ASTNode:127
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:134
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:136
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:145
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:146
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  if (getSpecies_handler != null) {
    getSpecies_handler.cleanupListeners();
  }
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:157
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:158
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   */
  protected ASTNode$DepGraphNode getSpecies_handler = ASTNode$DepGraphNode.createAstHandler(this, "getSpecies", null);
  /**
   * Replaces the lexeme Species.
   * @param value The new value for the lexeme Species.
   * @apilevel high-level
   */
  public Goat setSpecies(int value) {
    tokenint_Species = value;
    
    if (state().disableDeps == 0 && !state().IN_ATTR_STORE_EVAL) {
      getSpecies_handler.notifyDependencies();
    
    
    
    
    }
    return this;
  }
  /**
   * Retrieves the value for the lexeme Species.
   * @return The value for the lexeme Species.
   * @apilevel high-level
   */
  @ASTNodeAnnotation.Token(name="Species")
  public int getSpecies() {
    
    state().addHandlerDepTo(getSpecies_handler);
    return tokenint_Species;
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
