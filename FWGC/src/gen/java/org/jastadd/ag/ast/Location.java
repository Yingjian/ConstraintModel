/* This file was generated with JastAdd2 (http://jastadd.org) version 2.3.5 */
package org.jastadd.ag.ast;
import java.util.*;
/**
 * @ast node
 * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\ag.ast:44
 * @astdecl Location : ASTNode ::= [Farmer] [Wolf] [Goat] [Cabbage];
 * @production Location : {@link ASTNode} ::= <span class="component">[{@link Farmer}]</span> <span class="component">[{@link Wolf}]</span> <span class="component">[{@link Goat}]</span> <span class="component">[{@link Cabbage}]</span>;

 */
public abstract class Location extends ASTNode<ASTNode> implements Cloneable {
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:519
   */
  public static Location createRef(String ref) {
    Unresolved$North unresolvedNode = new Unresolved$North();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(true);
    return unresolvedNode;
  }
  /**
   * @aspect ReferenceCreation
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:525
   */
  public static Location createRefDirection(String ref) {
    Unresolved$North unresolvedNode = new Unresolved$North();
    unresolvedNode.setUnresolved$Token(ref);
    unresolvedNode.setUnresolved$ResolveOpposite(false);
    return unresolvedNode;
  }
  /**
   * @aspect ResolverTrigger
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:809
   */
  public void resolveAll() {
    super.resolveAll();
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:2069
   */
  Unresolved$Node$Interface as$Unresolved() {
    return null;
  }
  /**
   * @aspect RefResolverHelpers
   * @declaredat E:\\project\\ConstraintModel\\FWGC\\src\\gen\\jastadd\\agRefResolver.jadd:2075
   */
  boolean is$Unresolved() {
    return false;
  }
  /**
   * @declaredat ASTNode:1
   */
  public Location() {
    super();
  }
  /**
   * Initializes the child array to the correct size.
   * Initializes List and Opt nta children.
   * @apilevel internal
   * @ast method
   * @declaredat ASTNode:10
   */
  public void init$Children() {
    children = new ASTNode[4];  getChild_handler = new ASTNode$DepGraphNode[children.length];
    state().enterConstruction();
    setChild(new Opt(), 0);
    setChild(new Opt(), 1);
    setChild(new Opt(), 2);
    setChild(new Opt(), 3);
    state().exitConstruction();
  }
  /**
   * @declaredat ASTNode:19
   */
  @ASTNodeAnnotation.Constructor(
    name = {"Farmer", "Wolf", "Goat", "Cabbage"},
    type = {"Opt<Farmer>", "Opt<Wolf>", "Opt<Goat>", "Opt<Cabbage>"},
    kind = {"Opt", "Opt", "Opt", "Opt"}
  )
  public Location(Opt<Farmer> p0, Opt<Wolf> p1, Opt<Goat> p2, Opt<Cabbage> p3) {
state().enterConstruction();
    setChild(p0, 0);
    setChild(p1, 1);
    setChild(p2, 2);
    setChild(p3, 3);
state().exitConstruction();
  }
  /** @apilevel low-level 
   * @declaredat ASTNode:33
   */
  protected int numChildren() {
    
    state().addHandlerDepTo(numChildren_handler);
    return 4;
  }
  /**
   * @apilevel internal
   * @declaredat ASTNode:41
   */
  public boolean mayHaveRewrite() {
    return false;
  }
  /** @apilevel internal 
   * @declaredat ASTNode:45
   */
  public void flushAttrCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:48
   */
  public void flushCollectionCache() {
  }
  /** @apilevel internal 
   * @declaredat ASTNode:51
   */
  public Location clone() throws CloneNotSupportedException {
    Location node = (Location) super.clone();
    return node;
  }
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @deprecated Please use treeCopy or treeCopyNoTransform instead
   * @declaredat ASTNode:62
   */
  @Deprecated
  public abstract Location fullCopy();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:70
   */
  public abstract Location treeCopyNoTransform();
  /**
   * Create a deep copy of the AST subtree at this node.
   * The subtree of this node is traversed to trigger rewrites before copy.
   * The copy is dangling, i.e. has no parent.
   * @return dangling copy of the subtree at this node
   * @apilevel low-level
   * @declaredat ASTNode:78
   */
  public abstract Location treeCopy();
  /** @apilevel internal 
   * @declaredat ASTNode:80
   */
  protected boolean childIsNTA(int index) {
    return super.childIsNTA(index);
  }
  /**
   * @declaredat ASTNode:83
   */
  protected void inc_copyHandlers(Location copy) {
    super.inc_copyHandlers(copy);

  }
  /** @apilevel internal 
   * @declaredat ASTNode:89
   */
  public void reactToDependencyChange(String attrID, Object _parameters) {
    super.reactToDependencyChange(attrID, _parameters);
  }
  /**
   * @declaredat ASTNode:96
   */
  private boolean inc_throwAway_visited = false;
  /** @apilevel internal 
   * @declaredat ASTNode:98
   */
  public void inc_throwAway() {
  if (inc_throwAway_visited) {
    return;
  }
  inc_throwAway_visited = true;
  inc_state = inc_GARBAGE;
  super.inc_throwAway();
  inc_throwAway_visited = false;
}
  /**
   * @declaredat ASTNode:107
   */
  private boolean inc_cleanupListeners_visited = false;
  /**
   * @declaredat ASTNode:108
   */
  public void cleanupListeners() {
  if (inc_cleanupListeners_visited) {
    return;
  }
  inc_cleanupListeners_visited = true;
  super.cleanupListeners();
  inc_cleanupListeners_visited = false;
}
  /**
   * @declaredat ASTNode:116
   */
  private boolean inc_cleanupListenersInTree_visited = false;
  /**
   * @declaredat ASTNode:117
   */
  public void cleanupListenersInTree() {
  if (inc_cleanupListenersInTree_visited) {
    return;
  }
  inc_cleanupListenersInTree_visited = true;
  cleanupListeners();
  for (int i = 0; children != null && i < children.length; i++) {
    ASTNode child = children[i];
    if (child == null) {
      continue;
    }
    child.cleanupListenersInTree();
  }
  inc_cleanupListenersInTree_visited = false;
}
  /**
   * Replaces the optional node for the Farmer child. This is the <code>Opt</code>
   * node containing the child Farmer, not the actual child!
   * @param opt The new node to be used as the optional node for the Farmer child.
   * @apilevel low-level
   */
  public Location setFarmerOpt(Opt<Farmer> opt) {
    setChild(opt, 0);
    return this;
  }
  /**
   * Replaces the (optional) Farmer child.
   * @param node The new node to be used as the Farmer child.
   * @apilevel high-level
   */
  public Location setFarmer(Farmer node) {
    getFarmerOpt().setChild(node, 0);
    return this;
  }
  /**
   * Check whether the optional Farmer child exists.
   * @return {@code true} if the optional Farmer child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  public boolean hasFarmer() {
    return getFarmerOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) Farmer child.
   * @return The Farmer child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  public Farmer getFarmer() {
    return (Farmer) getFarmerOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the Farmer child. This is the <code>Opt</code> node containing the child Farmer, not the actual child!
   * @return The optional node for child the Farmer child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="Farmer")
  public Opt<Farmer> getFarmerOpt() {
    return (Opt<Farmer>) getChild(0);
  }
  /**
   * Retrieves the optional node for child Farmer. This is the <code>Opt</code> node containing the child Farmer, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child Farmer.
   * @apilevel low-level
   */
  public Opt<Farmer> getFarmerOptNoTransform() {
    return (Opt<Farmer>) getChildNoTransform(0);
  }
  /**
   * Replaces the optional node for the Wolf child. This is the <code>Opt</code>
   * node containing the child Wolf, not the actual child!
   * @param opt The new node to be used as the optional node for the Wolf child.
   * @apilevel low-level
   */
  public Location setWolfOpt(Opt<Wolf> opt) {
    setChild(opt, 1);
    return this;
  }
  /**
   * Replaces the (optional) Wolf child.
   * @param node The new node to be used as the Wolf child.
   * @apilevel high-level
   */
  public Location setWolf(Wolf node) {
    getWolfOpt().setChild(node, 0);
    return this;
  }
  /**
   * Check whether the optional Wolf child exists.
   * @return {@code true} if the optional Wolf child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  public boolean hasWolf() {
    return getWolfOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) Wolf child.
   * @return The Wolf child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  public Wolf getWolf() {
    return (Wolf) getWolfOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the Wolf child. This is the <code>Opt</code> node containing the child Wolf, not the actual child!
   * @return The optional node for child the Wolf child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="Wolf")
  public Opt<Wolf> getWolfOpt() {
    return (Opt<Wolf>) getChild(1);
  }
  /**
   * Retrieves the optional node for child Wolf. This is the <code>Opt</code> node containing the child Wolf, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child Wolf.
   * @apilevel low-level
   */
  public Opt<Wolf> getWolfOptNoTransform() {
    return (Opt<Wolf>) getChildNoTransform(1);
  }
  /**
   * Replaces the optional node for the Goat child. This is the <code>Opt</code>
   * node containing the child Goat, not the actual child!
   * @param opt The new node to be used as the optional node for the Goat child.
   * @apilevel low-level
   */
  public Location setGoatOpt(Opt<Goat> opt) {
    setChild(opt, 2);
    return this;
  }
  /**
   * Replaces the (optional) Goat child.
   * @param node The new node to be used as the Goat child.
   * @apilevel high-level
   */
  public Location setGoat(Goat node) {
    getGoatOpt().setChild(node, 0);
    return this;
  }
  /**
   * Check whether the optional Goat child exists.
   * @return {@code true} if the optional Goat child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  public boolean hasGoat() {
    return getGoatOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) Goat child.
   * @return The Goat child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  public Goat getGoat() {
    return (Goat) getGoatOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the Goat child. This is the <code>Opt</code> node containing the child Goat, not the actual child!
   * @return The optional node for child the Goat child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="Goat")
  public Opt<Goat> getGoatOpt() {
    return (Opt<Goat>) getChild(2);
  }
  /**
   * Retrieves the optional node for child Goat. This is the <code>Opt</code> node containing the child Goat, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child Goat.
   * @apilevel low-level
   */
  public Opt<Goat> getGoatOptNoTransform() {
    return (Opt<Goat>) getChildNoTransform(2);
  }
  /**
   * Replaces the optional node for the Cabbage child. This is the <code>Opt</code>
   * node containing the child Cabbage, not the actual child!
   * @param opt The new node to be used as the optional node for the Cabbage child.
   * @apilevel low-level
   */
  public Location setCabbageOpt(Opt<Cabbage> opt) {
    setChild(opt, 3);
    return this;
  }
  /**
   * Replaces the (optional) Cabbage child.
   * @param node The new node to be used as the Cabbage child.
   * @apilevel high-level
   */
  public Location setCabbage(Cabbage node) {
    getCabbageOpt().setChild(node, 0);
    return this;
  }
  /**
   * Check whether the optional Cabbage child exists.
   * @return {@code true} if the optional Cabbage child exists, {@code false} if it does not.
   * @apilevel high-level
   */
  public boolean hasCabbage() {
    return getCabbageOpt().getNumChild() != 0;
  }
  /**
   * Retrieves the (optional) Cabbage child.
   * @return The Cabbage child, if it exists. Returns {@code null} otherwise.
   * @apilevel low-level
   */
  public Cabbage getCabbage() {
    return (Cabbage) getCabbageOpt().getChild(0);
  }
  /**
   * Retrieves the optional node for the Cabbage child. This is the <code>Opt</code> node containing the child Cabbage, not the actual child!
   * @return The optional node for child the Cabbage child.
   * @apilevel low-level
   */
  @ASTNodeAnnotation.OptChild(name="Cabbage")
  public Opt<Cabbage> getCabbageOpt() {
    return (Opt<Cabbage>) getChild(3);
  }
  /**
   * Retrieves the optional node for child Cabbage. This is the <code>Opt</code> node containing the child Cabbage, not the actual child!
   * <p><em>This method does not invoke AST transformations.</em></p>
   * @return The optional node for child Cabbage.
   * @apilevel low-level
   */
  public Opt<Cabbage> getCabbageOptNoTransform() {
    return (Opt<Cabbage>) getChildNoTransform(3);
  }
  /** @apilevel internal */
  public ASTNode rewriteTo() {
    return super.rewriteTo();
  }
  /** @apilevel internal */
  public boolean canRewrite() {
    return false;
  }

}
