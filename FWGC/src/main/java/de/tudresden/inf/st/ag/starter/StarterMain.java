package de.tudresden.inf.st.ag.starter;
import org.jastadd.ag.ast.*;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.nio.charset.StandardCharsets;

public class StarterMain {
  public static void main(String[] args){
	  System.out.println("Input the following number for different Cases.");
	  System.out.println("0:Only the farmer ferries.");
	  System.out.println("1:The farmer ferries with the wolf.");
	  System.out.println("2:The farmer ferries with the goat.");
	  System.out.println("3:The farmer ferries with the cabbage.");
	    play();
    }
	public static void play() {	    
	    FWGC game = new FWGC();
		Constraint_Model CM=new Constraint_Model();
	    game.init();
		while(!game.end()){
			game.print();
			int Case = -1;
    		Scanner sc=new Scanner(System.in);
	    	while(Case < 0 || Case >3){//input #disks
	    		String s=sc.next();
	      		Case = checkInt(s);
			}
			Constraint_Model cm= new Constraint_Model();
			if(Case==0){
				if(game.getSouth().hasFarmer()){
					//if(cm.Valid_Move_1(game.getSouth())){
						if(game.move(game.getSouth(), game.getNorth(), Case)==false){
							continue;
						}
					//}else{continue;}	
				}else{
					//if(cm.Valid_Move_1(game.getNorth())){
						if(game.move(game.getNorth(), game.getSouth(), Case)==false){
							continue;
						}
					//}else{continue;}
				}
			}else{
				if(game.getSouth().hasFarmer()){
					//if(cm.Valid_Move_2(game.getSouth())){
						game.move(game.getSouth(), game.getNorth(), Case);
					//}else{continue;}	
				}else{
					//if(cm.Valid_Move_2(game.getNorth())){
						game.move(game.getNorth(), game.getSouth(), Case);
					//}else{continue;}
				}
			}
		}
		game.print();
	    System.out.println("Game done.");
	    
  }
      public static int checkInt(String str){
        int a =0;
        try {
          a = Integer.parseInt(str);
          } catch (NumberFormatException e) {
            return 0;
          }
        return a;
      }
}